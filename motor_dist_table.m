% Print a table to convert a number in cm to a voltage VRef for the angular
% sensor of the furstrating box

%Distances in integer centimeters.

filename = "dist_table.s";

n_adc = 12;
vcc = 3.3;
d_max = 63;
v_max = 0.7;
v_min = 1.3;
d_thresh = 20; %if d lower than that, go to v_min
thresh_value = 1.35;

v_outside_range = 0.7;

if(nextpow2(d_max) > 8)
    error("d_max is too high (does not fit in 8 bits)")
end

out_file = fopen(filename, 'w');
fprintf(out_file, "; MATLAB GENERATED FILE.\n; See motor_dist_table.m\n");
fprintf(out_file, "processor\tpic16lf1789\n");
fprintf(out_file, "GLOBAL\tdist_table\nGLOBAL\tAdMotorVrefH\nGLOBAL\tAdMotorVrefL\n\n");
fprintf(out_file, "psect dist_table__code,class=CODE,space=SPACE_CODE,delta=2\n");
fprintf(out_file, "dist_table:\n");
fprintf(out_file, "    andlw\t%sB\n    brw\n", dec2bin(pow2(nextpow2(d_max+1))-1, 8));

for i = 0:d_max
    fprintf(out_file, "        goto\tmotor_%d_cm\n", i);
end

for i = d_max:pow2(nextpow2(d_max+1))-2
    fprintf(out_file, "        goto\ttoo_far\n");
end

voltages = linspace(v_min, v_max, d_max);
voltages = round(voltages ./ vcc .* 2^n_adc);
thresh_value = round(thresh_value / vcc * 2^n_adc);
for i = d_min:d_max
    fprintf(out_file, "motor_%d_cm:\n", i);
    if(i > d_thresh)
        v = dec2bin(voltages(i), 16);
    else
        v = dec2bin(thresh_value, 16);
    end
    fprintf(out_file, "        movlw\t%sB\n", v(1:8));
    fprintf(out_file, "        movwf\tBANKMASK(AdMotorVrefH)\n");
    fprintf(out_file, "        movlw\t%sB\n", v(9:16));
    fprintf(out_file, "        movwf\tBANKMASK(AdMotorVrefL)\n");
    fprintf(out_file, "    return\n");
end

v_outside_range = round(v_outside_range ./ vcc .* 2^n_adc);
v_outside_range = dec2bin(v_outside_range, 16);
if(d_max ~= pow2(nextpow2(d_max+1))-1)
    
    fprintf(out_file, "out_of_range:\n"); 
    fprintf(out_file, "        movlw\t%sB\n", v_outside_range(1:8));
    fprintf(out_file, "        movwf\tBANKMASK(AdMotorVrefH)\n");
    fprintf(out_file, "        movlw\t%sB\n", v_outside_range(9:16));
    fprintf(out_file, "        movwf\tBANKMASK(AdMotorVrefL)\n");
    fprintf(out_file, "    return\n");
end

fprintf(out_file, "END");
fclose(out_file);