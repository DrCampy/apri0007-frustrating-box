%Circuit parameters:
L = 44.3e-6;
Rl = 14e-3;
C = 150e-6;
R = 4;
fs = 30e3;
Ts = 1/fs;
Vo = 6;
Vin = 3.3;
D = 0.45;
Il = Vin/( (1-D)^2*R);

% Small signal closed loop gain:
Gvd = tf( [ -Vo/(1-D) * (Il*L)/((1-D)*Vo) , Vo/(1-D) * ( -(Rl*Il)/((1-D)*Vo) +1 ) ], [ L*C/(1-D)^2 , Rl*C/(1-D)^2 , 1 ]);

% Delay:
n = 2;
td = n * Ts;
Gdelay = tf(1,1,'InputDelay',td);

% Voltage sensor:

Gsensor = tf(1/2,1);

% PID DESIGN : 

wco = 2 * pi * fs/10;  % Crossover frequency
pm = 52;               % Phase margin
wPI = wco/10;
nbPeriods = 1;         % Number of Ts used for the control code
T = nbPeriods * Ts;
wp = 2/T;


% Find wPD: 

temp = evalfr (Gvd, j*wco);
phaseGvd = atan(imag(temp)/real(temp) ) * 180 / pi;
phaseDelay = - wco * td * 180 / pi;
phasePI = atan(- wPI/wco) * 180 / pi;
phaseWP = -atan(wco/wp) * 180 / pi;

syms wPD
eqn = atan(wco/wPD)*180/pi == -180 + pm - phaseGvd - phaseDelay - phasePI - phaseWP;

wPD = vpasolve(eqn,wPD,[1;1e7]);
wPD = double(wPD);

% Find gain:

GPI = tf([1,wPI],[1,0]);
GPD = tf([1/wPD , 1],[1/wp , 1]);

GPID = GPI*GPD;
GainPID = abs(evalfr(GPID,j*wco));
GainDelay = 1;
GainGvd = abs(evalfr(Gvd,j*wco));
GainSensor = abs(evalfr(Gsensor,j*wco));

K = 1/ (GainPID*GainDelay*GainGvd*GainSensor);

Kp = K * (1 + wPI/wPD - 2*wPI/wp);
Ki = 2*K*wPI/wp;
Kd = 0.5 * K * (1 - wPI/wp) * (wp/wPD - 1);

%Rouded to the closest power of 2:

floorKp = sign(Kp) * 2^(floor(log2(abs(Kp))));
ceilKp = sign(Kp) * 2^(ceil(log2(abs(Kp))));

if (abs(floorKp-Kp) <= abs(ceilKp-Kp))
    Kp2 = floorKp;
else
    Kp2 = ceilKp;
end

floorKi = sign(Ki) * 2^(floor(log2(abs(Ki))));
ceilKi = sign(Ki) * 2^(ceil(log2(abs(Ki))));

if (abs(floorKi-Ki) <= abs(ceilKi-Ki))
    Ki2 = floorKi;
else
    Ki2 = ceilKi;
end

floorKd = sign(Kd) * 2^(floor(log2(abs(Kd))));
ceilKd = sign(Kd) * 2^(ceil(log2(abs(Kd))));

if (abs(floorKd-Kd) <= abs(ceilKd-Kd))
    Kd2 = floorKd;
else
    Kd2 = ceilKd;
end

sentence = ['Kp =  ',num2str(Kp),' is rounded to Kp2 = ',num2str(Kp2),' => corresponding power of 2:  ',num2str(log2(abs(Kp2)))];
disp(sentence);
sentence = ['Ki =  ',num2str(Ki),' is rounded to Ki2 = ',num2str(Ki2),' => corresponding power of 2:  ',num2str(log2(abs(Ki2)))];
disp(sentence);
sentence = ['Kd =  ',num2str(Kd),' is rounded to Kd2 = ',num2str(Kd2),' => corresponding power of 2:  ',num2str(log2(abs(Kd2)))];
disp(sentence);








