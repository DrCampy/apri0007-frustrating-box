; *********************************************************** ;
;                  PID FOR BOOST CONVERTER                    ;
;             Reads ADC value, compares with ref              ;
;		  and generates a duty cycle                  ;
;						              ;
;            APRI0007 - Major Project in electronics          ;
;                                                             ;
; *********************************************************** ;
   
 
    processor 16f1789
    #include	<p16lf1789.inc>
    
    ERRORLEVEL 	-302  ; To remove the "not in bank0" message
    
    ; Vref = 3V on ADC 12-bits:
    #DEFINE	VrefH	    b'00001110' 
    #DEFINE	VrefL	    b'10001011'
    #DEFINE	BoostPIDSat 0
    
    ; Parameters for the configurations registers.
    ; CONFIG1
    ; __config 0x2F82
     __CONFIG _CONFIG1, _FOSC_INTOSC & _WDTE_OFF & _PWRTE_ON & _MCLRE_OFF & _CP_OFF & _CPD_OFF & _BOREN_ON & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_ON
    ; CONFIG2
    ; __config 0x1EFF
     __CONFIG _CONFIG2, _WRT_OFF & _PLLEN_OFF & _STVREN_ON & _BORV_LO & _LPBOR_OFF & _LVP_OFF
    
    
; That is where the MCU will start executing the program (0x00)
	org 	0x00
	nop
	goto    start	 ; jump to the beginning of the code
	
	
;Give the address of the interrupt routine to the interrupt vector
	org 0x04 
	goto interrupt

;BEGINNING OF THE PROGRAM
start:
	call	initialisation    ; initialisation routine configuring the MCU
	goto	main_loop         ; main loop

;INITIALISATION
initialisation:
    ; Memory organisation:
    cblock 0xA0 ; replace number of adress in bank1 by a label
	Flags
	AdBoostVrefH
	AdBoostVrefL
	AdBoostErrH ; error at time t
	AdBoostErrL
	AdBoostPrevErrH ; error at time t-1
	AdBoostPrevErrL
	AdBoostPropH ; Proportional term
	AdBoostPropL
	AdBoostCumH ; Integrational term
	AdBoostCumL
	AdBoostDiffH ; Derivative term
	AdBoostDiffL
	AdBoostPIDResH ; Sum of 3 terms
	AdBoostPIDResL
    endc
    
    ; Enable interrupts:
	banksel PIR1
	
	clrf	PIR1 ; clear interrupt flags before enabling interupts.
	
	movlw   b'11000000' ;enable global and peripheral interupts.
	movwf   INTCON
	
	banksel PIE1
	movlw   b'01000001'
	iorwf	PIE1,1	; OR operation to not affect the other bits.
	
    ; Clear personnal flags:
	clrf	Flags

    ; I/O configuration
	banksel TRISA
	movlw   b'00000001'  
	movwf   TRISA      ; Only RA0 is an input (for ADC)       
	banksel LATA
	clrf	LATA    ; all outputs are zero         
	
    ; Configuration of clock - freq = 4MHZ - internal oscillator block 
	banksel OSCCON
	movlw	b'01101110'
	movwf	OSCCON	
	banksel OSCTUNE
	movlw	b'00000000'	    
	movwf	OSCTUNE	    
    
    ; Timer1 ON - 1:1 prescaling -  Fosc/1 clock source
	banksel T1CON
	movlw	b'01000001'
	movwf	T1CON       ; configure Timer1 (cf. datasheet SFR T1CON)
	
    ; Configuration of ADC:
	banksel ADCON0
	movlw   b'00000001' ; 12 bits result, enabled
	movwf	ADCON0 
	banksel ADCON1
	movlw   b'11000000' ; 2's Complement -  Fosc/4 - Vref = VDD/VSS
	movwf	ADCON1 ;
	
    ; Configuration of PWM signal:
	; Set period to 33.33s with a 64MHz clk:
	    banksel PSMC1CON
	    movlw   b'00001000'
	    movwf   PSMC1PRH
	    movlw   b'01010101'
	    movwf   PSMC1PRL
	; No offset:
	    clrf    PSMC1PHH   
	    clrf    PSMC1PHL
	; Initialize with D = 0% [rising edge = falling edge]:
	    clrf    PSMC1DCH   
	    clrf    PSMC1DCL 
	; Clock frequency = 64MHz:
	    movlw   b'00000001' 
	    movwf   PSMC1CLK
	; Set output on A with normal polarity:
	    bsf	    PSMC1STR0,	P1STRA
	    bcf	    PSMC1POL,	P1POLA
	    bsf	    PSMC1OEN,	P1OEA
	; Set time base event for all events:
	    bsf	    PSMC1PRS,	P1PRST
	    bsf	    PSMC1PHS,	P1PHST	
	    bsf	    PSMC1DCS,	P1DCST
	; Enable the PWM and the output pin:
	    movlw   b'11000000' ; No deadbands and single PWM waveform
	    movwf   PSMC1CON
	    banksel TRISC
	    bcf	    TRISC, 0 ; pinA of PSMC1 corresponds to RC0
	
    ; Initialize Timer1 to trigger interrupt at 200s [f=5kHz]                     
	movlb   0x00	
	bcf	T1CON, TMR1ON
	movlw	b'11111100'
	movwf	TMR1H
	movlw	b'11100000'
	movwf	TMR1L
	bsf	T1CON, TMR1ON
	
    ; Storing reference in memory:
	movlb   0x01
	movlw   VrefH
	movwf   AdBoostVrefH	
	movlw   VrefL
	movwf   AdBoostVrefL
    
    ; Setting previous error to 0 for D part of PID
	clrf   AdBoostPrevErrH
	clrf   AdBoostPrevErrL
    
    ; Setting cumulated error to 0 for I part of PID
	clrf  AdBoostCumH
	clrf  AdBoostCumL
    
    return

;INTERRUPT ROUTINE
interrupt:
    
    movlb   0x00
    btfss   PIR1, TMR1IF   
    ; Check ADC if the Timer1 overflow flag is not set
	goto	boost_ADC_interrupt

	bcf	PIR1, TMR1IF
    
    ; Reset the counter to get next interrupt 200ms later (f = 5kHz)
	banksel T1CON	
	bcf	T1CON, TMR1ON
	movlw	b'11111100'
	movwf	TMR1H
	movlw	b'11100000'
	movwf	TMR1L
	bsf	T1CON, TMR1ON
    
    ; Start ADC conversion
	banksel ADCON0
	bsf	ADCON0, ADGO
    
boost_ADC_interrupt:
    banksel PIR1
    btfss PIR1, ADIF 
    ; Leave interrupt routine if ADC flag is not set
	goto end_of_interrupt
    
    bcf	  PIR1, ADIF ; Clear ADC interrupt f lag
    
    ; Computation of the error e = Vref - Vadc:
	movlb	0x01
	movf    ADRESL, 0   
	subwf   AdBoostVrefL, 0
	movwf   AdBoostErrL
	movf    ADRESH, 0   
	subwfb  AdBoostVrefH, 0
	movwf   AdBoostErrH  
    
    ; PID Calculation:
	; Kp = 0.015625 -> Kp* ~= 2^-5 (Adjusted to digital) 
	movf    AdBoostErrH,	0 
	movwf   AdBoostPropH
	movf    AdBoostErrL,	0 
	movwf   AdBoostPropL
	asrf	AdBoostPropH, 1  ; We do it five times (faster than loop)
	rrf	AdBoostPropL, 1
	asrf	AdBoostPropH, 1
	rrf	AdBoostPropL, 1
	asrf	AdBoostPropH, 1
	rrf	AdBoostPropL, 1
	asrf	AdBoostPropH, 1
	rrf	AdBoostPropL, 1
	asrf	AdBoostPropH, 1
	rrf	AdBoostPropL, 1
	
	; Ki * Int:
	;if sat = 1, we directly jump to the derivative term
	btfsc	Flags, BoostPIDSat
	goto	boost_derivative_term
	
	; Ki*e[k] = Kp*e[k]: we can just add the proportional term 
	; to the cumulative sum
	movf	AdBoostPropL, 0
	addwf	AdBoostCumL, 1
	movf	AdBoostPropH, 0
	addwfc	AdBoostCumH, 1
	btfsc   AdBoostPropH, 7  ;if MSB = 0 and Carry = 1 -> Overflow
	    goto boost_derivative_term
	btfss STATUS, C
	    goto boost_derivative_term
	    
	movlw	b'1111111' ;if overflow, set to max value
	movwf	AdBoostCumH
	movwf	AdBoostCumL
	  
	    
boost_derivative_term:
	;Kd = 0.125 -> Kd* ~= 2^-2 (Adjusted to digital) 
	; Calculation of DeltaE[k] = e[k]- e[k-1]:
	movf	AdBoostPrevErrL, 0
	subwf	AdBoostErrL, 0
	movwf	AdBoostDiffL
	movf	AdBoostPrevErrH, 0
	subwfb	AdBoostErrH, 0
	movwf	AdBoostDiffH
	; Calculation of Kd * DeltaE[k]:
	asrf	AdBoostDiffH, 1  ; We do it two times (faster than loop)
	rrf	AdBoostDiffL, 1
	asrf	AdBoostDiffH, 1
	rrf	AdBoostDiffL, 1
	
	
	; Summate the three terms:
	movf	AdBoostCumL, 0
	addwf	AdBoostDiffL, 0
	movwf	AdBoostPIDResL
	movf	AdBoostCumH, 0
	addwfc	AdBoostDiffH, 0
	btfsc   AdBoostDiffH, 7  ;if MSB = 0(positive) and Carry = 1 -> Overflow
	    goto boost_continue_sum
	btfsc STATUS, C
	    goto boost_positive_sat
boost_continue_sum:
	movwf	AdBoostPIDResH
	movf	AdBoostPropL, 0
	addwf	AdBoostPIDResL, 1
	movf	AdBoostPropH, 0
	addwfc	AdBoostPIDResH, 1
	btfsc   AdBoostPropH, 7 ;if MSB = 0(positive) and Carry = 1 -> Overflow
	    goto boost_end_of_sum
	btfsc STATUS, C
	    goto boost_positive_sat
	
boost_end_of_sum:
    ; Duty cycle generation:
	; Check if PIDRes is negative: there was no overflow, if bit7=1 -> negative
	    btfss   AdBoostPIDResH, 7
	    goto    boost_duty_positive
	; if the MSB is 1, the output of controller is negative => D = 0:
	    bsf	    Flags, BoostPIDSat  ; Anti-windup
	    banksel PSMC1CON
	    clrf    PSMC1DCH   
	    clrf    PSMC1DCL 
	    goto    boost_update_prev_error
	
	; else:	
boost_duty_positive:
	    ;check if PIDRes > Period of PSMC1
	     
	   movf    AdBoostPIDResH, 0
	    banksel PSMC1CON
	    subwf   PSMC1PRH, 0
	    btfss   STATUS, C   
		goto    boost_positive_sat
		
	    btfss   STATUS, Z
		goto    boost_duty_cycle_ok 
		
	    movlb   0x01
	    movf    AdBoostPIDResL, 0
	    banksel PSMC1CON
	    subwf   PSMC1PRL, 0
	    btfss   STATUS, C   
		goto    boost_positive_sat
		
	    goto    boost_duty_cycle_ok 
	    
boost_positive_sat:
	    movlb   0x01
	    bsf	    Flags, BoostPIDSat  ; Anti-windup on
	    ;update duty-cycle to bigger value than period to have DC = 100%:
	    banksel PSMC1CON
	    movlw   b'11111111'
	    movwf   PSMC1DCH
	    goto    boost_update_prev_error
	    
	    ; else: sat = 0
boost_duty_cycle_ok: 
	    ; clear saturation flag
	    movlb   0x01
	    bcf	    Flags, BoostPIDSat  ; Anti-windup off
	      
	    ; Store the output of PID in Duty-cycle
	    movlb   0x01
	    movf    AdBoostPIDResL, 0
	    banksel PSMC1CON
	    movwf   PSMC1DCL
	    movlb   0x01
	    movf    AdBoostPIDResH, 0
	    banksel PSMC1CON
	    movwf   PSMC1DCH 
	   
    
boost_update_prev_error:
    ; Move error in prevError location for next computation:
	movlb	0x01
	movf	AdBoostErrH, 0 
	movwf   AdBoostPrevErrH
	movf    AdBoostErrL, 0 
	movwf   AdBoostPrevErrL  
    
end_of_interrupt:
    retfie
    	
;MAIN LOOP
main_loop:
    nop
    clrwdt ;clear watchDogTimer
    goto main_loop
    END  
