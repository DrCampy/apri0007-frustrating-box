;complementary single-phase PWM
;fully synchronous operation
;PWM period=5 microseconds
;PWM duty cycle= 45%
;clock = 4MHz

#include "config.inc"
#define CriricalAngleH
#define CriricalAngleL
#define VrefH
#define VrefL        
org 0x00
	nop
	goto start
org 0x04
	nop
	goto interrupt_routine
start:
      CALL initialisation
      goto  main_loop

initialisation:
    ;initialisation interrupts
    MOVLB 0x00
    MOVLW b'00000000'
    MOVWF PIR1          ;clear interrupts before enable it
      
    MOVLB 0x01
    MOVLW b'11000000'
    MOVWF INTCON        ;enable global and peripheral interrupts
       
    MOVLW b'01000001'   ;enable ADC and Timer1 interrupts
    MOVWF PIE1          ;bit6 for ADC ,bit0 for Timer1
    IORWF PIE1,1        ;or operation to not affect on other bits
    
    ;configuration of clock 4MHz internal oscillator
    ;MOVLB 0x01 (already in bank 1 :G)
    MOVLW b'01101010'
    MOVWF OSCCON
    MOVLW b'00000000'
    MOVWF OSCTUNE
    
    //configure PORTC        
       MOVLB 0x00     //bank select PORTC
       CLRF PORTC     //init portc
       CLRF LATC      //motor is stopped at first? (I think to stop the motor you should also disable the PSMC G:)
       MOVLB 0x03     //bank select  ANSELC 
       CLRF ANSELC
       MOVLB 0x01     //bank select TRISC  
       MOVLW b'00000000'   
       MOVWF TRISC    //RC<7:0> as outputs ,RC0 is H-bridge-A
                      //and RC1 is H-bridge-B
    
    //configure PORTA
       MOVLB 0x00     //bank select PORTA
       CLRF PORTA     //init PORTA
       CLRF LATA
       MOVLB 0x03     //bank select  ANSELA 
       ;CLRF ANSELA
       MOVLB 0x01     //bank select TRISA  
       MOVLW b'00000111'   
       MOVWF TRISA    //RA<7:4> as outputs ,RA2 is Angle-sense
                      //RA1 is V-sense and RA2 H-Bridge-EN (you mean Angle sensor ? G:)
              
    ;configuration PSMC
    
              
    banksel PSMC1CON      ;selecting bank 29 where all of PMCx registers are
    movlw b'00010100'      ;set PR of the pwm to 20 because Fpic = 4MHz -> Tpic = 0.25 microsec  Fpwm = 2000kHz then Tpwm = 500 microsec  
					       ;period PWM = 5/0.25 = 2000
    movwf PSMC1PRL
     
    ;set duty cycle
    CLRF PSMC1DCH ; higher register of DC not used
    movlw b'00001010'     ; set to 10 because 50% of 20 = 10
    movwf PSMC1DCL      

    ;no phase offset
    CLRF PSMC1PHH
    CLRF PSMC1PHL    

    ;configuration of PSMC1CLK  
    MOVLW b'00000000'  ;use the 4 MHz clock?
    MOVWF PSMC1CLK   

    ;output pin on A,normal polarity
    ;CLRF  PSMC1OEN not usefull to clear them if you change them after G:
    ;CLRF  PSMC1STR0        
    MOVLW b'00000011'
    MOVWF PSMC1OEN         ;enable outputs A and B 
    MOVWF PSMC1STR0        ;steering the PWM signal to A and B
    CLRF  PSMC1POL

    ;set time base as source of all events
    BSF PSMC1PRS,   P1PRST        
    BSF PSMC1PHS,   P1PHST
    BSF PSMC1DCS,   P1DCST

    ;clear dead band ?? (see if we should add deadband or not)
    MOVLW b'00000000'    
    MOVWF PSMC1DBR
    MOVWF PSMC1DBF
    ; if we still don't use deadband gain 1 op G:
    ;CLRF PSMC1DBR
    ;CLRF PSMC1DBF

    ;enable PSMC in single phase complementary
    ;CLRF  PSMC1CON  not usefull if you change it after G: 
    MOVLW b'11000001'   
    MOVWF PSMC1CON
    
    return
          
;PSMC interrupt routine
interrupt_PSMC:
    BTFSS PIRI,TMR1F   ; check timer1 interrupt flag
    goto end_if_timer1
     
    ;doesnt finish yet till obtain the values of critical angles

   
    ;ADC Configuration 
    MOVLW b'11010101'      ;bit7:10bits result-bit6-2:measure AN21 just for now
                           ;bit1:no measure yet-bit0:disable ADC
    MOVWF ADCON0
    
    MOVLW b'01000000'    ;bit7: sign magnitude-bit6-4:prescaler Fosc/2
                          ;bit2:Vref-=Vss - bit1-0:Vref+=Vdd
    MOVWF ADCON1 
          
    ;configure timer1 ON
    MOVLB 0x00
    MOVLW b'00110000'
    MOVWF TICON 
       
    ;initilize timer1 to trigger interrupt at 0.5ms 
    MOVLB 0x00
    BCF TICON,0
    MOVLB 0x00
    MOVLW 0xFF
    MOVWF TMR1H
    MOVLW 0xC1
    MOVWF TMR1L
    BSF   TICON,0  
                 
    ;ADC interrupt routine
    MOVLB 0x00           ;bank select
    BTFSS PIRI, 0       ;check if TMR1E is '1'
    goto ADC_interrupt   ;if not goto next interrupt test 
                 
              
    MOVLB 0x01
    BSF ADCON0,0        ;start conversion
    
    retfie 
 
ADC_interrupt:

    MOVLB 0x00
    BTFSS PIRI,6             ;check if ADC interrupt flag = 1
	retfie
            
    if(ADRESH > CriticalAngleH){
        turn off motor
    }
    //doesnt finish yet till obtain the values of critical angles
    
    retfie ;because interrupt retfie

main_loop:
    nop
    goto main_loop