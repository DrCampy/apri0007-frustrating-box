#include	<p16lf1789.inc>  

i2cvar	    udata H'220'	; Variables for I2C
i2c_state    res 1		; 1 byte
     
flags	    udata_shr		; Placed in the shared ram for faster access
flags1	    res 1		; 1 byte for the flags
last_dist   res 1		; 1 byte for the last distance read.
   
bank0_var   udata H'20'	
wait_sensor_init
	    res 1

	    
thresh	    equ D'20'		; LED light-up threshold
i2c_flag    equ D'0'		; I2C flag is bit 0 of reg flags1
fetch_flag  equ D'1'		; New measurement flag is bit 1 of reg flags1
i2cbrg	    equ H'09'		; Baud rate divider for 400kHz I2C
sensorAddr  equ H'42'		; distance sensor address
  
; Parameters for the configurations registers.
; CONFIG1
; __config 0x2F82
 __CONFIG _CONFIG1, _FOSC_INTOSC & _WDTE_OFF & _PWRTE_ON & _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_ON & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_ON
; CONFIG2
; __config 0x1EFF
 __CONFIG _CONFIG2, _WRT_OFF & _PLLEN_OFF & _STVREN_ON & _BORV_LO & _LPBOR_OFF & _LVP_OFF
    
 ;Send a byte through I2C and wait for completion.
i2c_sendByteWait macro databyte
	banksel SSP1CON		    
	movlw databyte		    
	movwf SSP1BUF
	banksel PIR1
	btfss PIR1, SSP1IF
	goto $-1
	bcf PIR1, SSP1IF
    endm
    
;This is where the PIC will start executing the program (0x00)
    org	H'00'
    nop
    goto	start		    ;Jump to beginning of the code
    
;This is where the PIC will jump whenever an interrupt is triggered (0x04)
    org	H'04'
    goto	interrupt_routines
    
 ;Beginning of the program
start:
    call	initialization
    goto	main_loop
  
 ;Initialization of the program
initialization:
    ; Configures the clock
    banksel	OSCCON
    movlw	b'01101110'
    movwf	OSCCON		    ; configure oscillator (Fosc = 4MHz)
    movlw	b'00000000'	    
    movwf	OSCTUNE		    ; configure oscillator 
    
    ; Clears the variables
    clrf flags1 
    clrf last_dist
        
    ; Configures timer 2 for 30 interrupts per second
    banksel T2CON
    movlw B'01000110'		    ; Postscale = 9 - Timer ON - Prescale = 16
    movwf T2CON
    movlw B'11100111'
    movwf PR2
    clrf TMR2
    
    banksel PIE1		    ; Enables timer 2 interrupts
    bsf PIE1, TMR2IE	
    
    ; Wait 5 full timer cycle before initializing sensor
    banksel PIR1
    movlw D'10'
    movwf wait_sensor_init
wait_before_timer:
    bcf PIR1, TMR2IF
    btfss PIR1, TMR2IF
    goto $-1
    bcf PIR1, TMR2IF
    decfsz wait_sensor_init
    goto wait_before_timer
    
    
    ; Init i2c module
    call i2c_setup
    
    ; Configures RC5 as output
    banksel TRISC
    bcf TRISC, RC5
    
    ; Enables global interrupts
    banksel INTCON
    bsf INTCON, GIE		    ; Enables global interrupts
    bsf INTCON, PEIE		    ; Enables peropheral interrupts
    return

interrupt_routines:		    ; This code manages the interrupts.
				    ; Interrupts should be tested in the order
				    ; of the most frequent to the less frequent.
				    ; First interrupt is boost.
    
    ;btfss INTCON, TMR0IF	    ; Checks for the boost interrupt
    ;goto i2c_int
    ;
    ; boost code
    ;
    ;goto end_isr
i2c_int:    
    banksel PIR1		    
    btfss PIR1, SSP1IF		    ; i2c interrupt
    goto timer2_int
    bcf PIR1, SSP1IF		    ; Clears interrupt flag
    bsf flags1, i2c_flag	    ; Sets the I2C flag
    goto end_isr
timer2_int:
    btfss PIR1, TMR2IF		    ; Timer 2 interrupt
    goto end_isr
    bcf PIR1, TMR2IF		    ; Clear interrupt flag
    bsf flags1, fetch_flag    
end_isr:
    retfie			    ; Always use retfie to return from interrupts
    
    ; Setup the i2c module and calls the sensor init routine
i2c_setup:
    ; Configures I2C
    ; SDA pin is RC3
    ; SCL pin is RC4
    banksel i2c_state
    clrf i2c_state

    ; Configures pins properly for SSP module
    banksel TRISC		    ; Data direction register
    bsf	TRISC, TRISC3		    ; RC3 is input  
    bsf	TRISC, TRISC4;		    ; RC4 is input
    banksel ANSELC		    ; Analog function on pins
    bcf	ANSELC, ANSC3		    ; RC3 is digital
    bcf	ANSELC, ANSC4		    ; RC4 is digital

    banksel SSP1CON		    ; Selects correct bank for SSP module
    movlw i2cbrg		    ; Sets I2C baud rate generator 
    movwf SSP1ADD		    ; SSPADD is BRG in I2C master mode
    movlw b'00101000'		    ; SSPEN = 1 (enables module) SSPM = 1000 (I2C master mode)
    movwf SSP1CON1

    
    call sensor_init		    ; Initialize sensor before enabling interrupts
				    ; (function gets executed once, do not need
				    ; interrupts)
				    
    banksel PIR1		    ; Clears i2c interrupt flag
    bcf PIR1, SSP1IF		    
    banksel PIE1		    ; Enables MSSP interrupts
    bsf PIE1, SSP1IE
    return
    
    ; Initialize the sensor (send configuration bits and read address)
sensor_init:
    ;send Start - 0x42 (addr) - 0x00(reg) - 0b11001100(data) - 0x01(reg) - 
    ; 0b00000000(data) - 0x12(reg) - Stop
    
    ; Send start condition
    banksel SSP1CON
    bsf SSP1CON2, SEN		    
    btfsc SSP1CON2, SEN		    ; wait for start condition to terminate
    goto $-1
    banksel PIR1		    ; Reset SSP interrupt flag
    bcf PIR1, SSP1IF
    
    ; Send sensor address
    i2c_sendByteWait sensorAddr
    banksel SSP1CON
    btfsc SSP1STAT, ACKDT	    ; Check if sensor acknowledged it's address
    goto sensor_init_err
        
    ; Send 0x00
    i2c_sendByteWait H'00'
    banksel SSP1CON
    btfsc SSP1STAT, ACKDT	    ; Check if sensor acknowledged data
    goto sensor_init_err
    
    ; Send 0b11001100	    
    i2c_sendByteWait B'11001100'
    banksel SSP1CON
    btfsc SSP1STAT, ACKDT	    ; Check if sensor acknowledged data
    goto sensor_init_err
    
    ; Send 0x01    
    i2c_sendByteWait H'01'
    banksel SSP1CON
    btfsc SSP1STAT, ACKDT	    ; Check if sensor acknowledged data
    goto sensor_init_err
    
    ; Send 0b00000000	    
    i2c_sendByteWait B'00000000'
    banksel SSP1CON
    btfsc SSP1STAT, ACKDT	    ; Check if sensor acknowledged data
    goto sensor_init_err
    
    ; Send 0x12
    i2c_sendByteWait H'12'
    banksel SSP1CON
    btfsc SSP1STAT, ACKDT	    ; Check if sensor acknowledged data
    goto sensor_init_err
    
    ; Send stop condition
    bsf SSP1CON2, PEN
    btfsc SSP1CON2, PEN		    ; wait for stop condition to terminate
    goto $-1
    bcf PIR1, SSP1IF
    banksel PIR1		    ; Reset SSP interrupt flag
    bcf PIR1, SSP1IF
sensor_init_err:    
    return

    ; Sets the flags to start a new measurement from the sensor.
    ; First test if the i2c is in correct state. If it is not no measurement
    ; will be fetched.
fetch_meas:
    banksel i2c_state
    movf i2c_state, F		    ; Move F to its location to test if it is 0
    btfsc STATUS, Z
    bsf flags1, i2c_flag
    bcf flags1, fetch_flag
    return
    
    ; I2C execution routine. Have it's own state machine to keep track of the 
    ; next step to perform.
i2c_exec:
    banksel i2c_state
    movf i2c_state, W
    brw
	goto i2c_send_start
	goto i2c_send_address
	goto i2c_read_mode    
	goto i2c_read_byte
	goto i2c_stop
	goto i2c_int_after_stop

    ; Send start condition
i2c_send_start:
    banksel SSP1CON
    bsf SSP1CON2, SEN
    incf i2c_state, F
    goto i2c_exec_end
    
    ; Send the address of the sensor on the bus
i2c_send_address:
    banksel SSP1CON
    btfsc SSP1CON1, WCOL	    ; Test for potential collision
    goto i2c_exec_err		    
    btfsc SSP1CON2, SEN		    ; Check that start condition is over
    goto i2c_exec_err
    movlw sensorAddr+1		    ; Send address+1 for read mode
    movwf SSP1BUF
    incf i2c_state, F		    ; Next state
    goto i2c_exec_end
    
    ; Check if sensor acked it's address and switch to receive mode
i2c_read_mode:
    banksel SSP1CON
    btfsc SSP1CON1, WCOL	    ; Test for potential collision
    goto i2c_exec_err		    
    btfsc SSP1CON2, ACKSTAT	    ; Check that slave acked it's address
    goto i2c_exec_err
    bsf SSP1CON2, RCEN
    incf i2c_state, F		    ; Next state
    goto i2c_exec_end
        
    ; Read the byte received from the sensor
i2c_read_byte:
    banksel SSP1CON
    btfsc SSP1CON1, WCOL	    ; Test for potential collision
    goto i2c_exec_err
    btfsc SSP1CON1, SSPOV	    ; Test for potential receive overflow
    goto i2c_exec_err
    movfw SSP1BUF		    ; Read distance
    movwf last_dist
    bsf SSP1CON2, ACKDT		    ; Send NACK
    bsf SSP1CON2, ACKEN
    incf i2c_state, F		    ; Next state
    goto i2c_exec_end
    
    ; Send stop condition
i2c_stop:
    banksel SSP1CON
    bsf SSP1CON2, PEN		    ; Send stop
    incf i2c_state, F
    goto i2c_exec_end
    
    ; One last interrupt is fired after the send bit have been sent
i2c_int_after_stop:
    clrf i2c_state
    goto i2c_exec_end
    
i2c_exec_err:
    clrf i2c_state		    ; Reset state machine
    movlw B'00101000'		    ; Reset module state
    movwf SSP1CON1
i2c_exec_end:
    bcf flags1, i2c_flag
    return
    
main_loop:
    ; We now test the possible flags to branch to the correct parts.
    btfsc flags1, fetch_flag
    call fetch_meas
    
    btfsc flags1, i2c_flag	    ; Test if the i2c flag have been set
    call i2c_exec
    
    ; Checks whether or not to light the LED
    banksel LATC
    movf last_dist, W
    sublw thresh
    
    btfss STATUS, C 
    bcf LATC, RC5
    btfsc STATUS, C
    bsf LATC, RC5
    
    CLRWDT			    ; Clears watchdog timer
    goto main_loop
    end