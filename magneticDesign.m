clear;
%% Boost converter ideal parameters:

% Inputs:
Vg = 3.3; %[V] (generator)
V = 6; %[V] (required for the motor)
R = 4; %[Omh] (depends on the motor and its features at V)
fs = 30e3; %[Hz] 
Ts = 1/fs;

deltaV = 0.1; %V (Choice)
deltaIL_rel = 0.2; %[-] means deltaIL = 0.2 IL 

D = 1 - Vg/V; %[-]
sentence = ['The required duty cycle is ',num2str(D),'.'];
disp(sentence);

IL = Vg/( (1-D)^2 * R); %[Amp]
deltaIL = deltaIL_rel * IL;  %[Amp]
L = Vg / (2*deltaIL) * D * Ts; %[H]
C = V / (2*R*deltaV) * D *Ts;  %[F]

sentence = ['The parameters of the model are: R = ',num2str(4),' Omh, L = ',num2str(L*1e3),' mH and C = ',num2str(C*1e6),' uF.'];
disp(sentence);


%% Design of magnetic:
%We will often consider a factor x2 in the max values applied to the SC
%components in order to be safe for transcient effects.
fprintf('\nHere we are going to design the inductor:\n');

mu0 = 4*pi*10^-7;

Imax = IL + deltaIL;  %[A] - Highest current to go throught the transistor and the diode

%The material used ihas reference 3C90, Bsat = 470mT at 25�, we take a
%margin => 400mT
Bmax = 400e-3;       %[Tesla] 

rho = 1.72e-6;   % [omh.cm] Resistivity of copper

LossPercent = 4;
Pmax = LossPercent/100 * Vg * IL;
RL = Pmax/Imax^2; %[omh] = P/I^2
Ku = 0.8; % fraction of the core window filled by copper (<1) [0.5 for low voltage inductors =w]
    %peut augmenter

Kg = rho * L^2 * Imax^2 / (Bmax^2 * RL * Ku)* 1e8; %the 1e8 term is here for units purpose

sentence = ['You need to choose Kg >= ',num2str(Kg),'.'];
disp(sentence);

% Seems to be EE22 [Only the ETD designs are available, ETD34 is not there anymore]
% The chosen design gives:
Ac = 0.76 ; %[cm^2] cross section of the winding
Wa = 0.903; %[cm^2] bobine winding area
MLT = 5.33; %[cm] mean length per turn


lg = mu0 * L * Imax^2 / (Bmax^2 * Ac) * 1e4; %[m] length of the air gap

% Design the number of turns:
n = ceil(L * Imax / (Bmax * Ac)*1e4);
sentence = ['Numbers of turns n = ', num2str(n)];
disp(sentence);

Aw = Ku * Wa / n;

sentence = ['You need to choose AWG such that Aw <= ',num2str(Aw*1e3),'*10^-3 cm^'];
disp(sentence);

% From the table, it seems to be: AWG#22, we can hence change the real value
% for Aw:

Aw = 3.243e-3;

%Final step: check the losses:
Rreal = rho * n * MLT / Aw;

if (Rreal <= RL)
   sentence = ['The design is ok, the losses are not higher than expected you wanted RL =  ',num2str(RL),' Omh but you obtained Rreal = ',num2str(Rreal),' Omh.'];
   disp(sentence);
else 
    sentence = ['=> The design you made increased the losses, you wanted RL =  ',num2str(RL),' Omh but you obtained Rreal = ',num2str(Rreal),' Omh.'];
    disp(sentence);
end

%% SC components:
fprintf('\nWe will now consider information for the SC components:\n');
sentence1 = 'The SC components will have to be chosen such that they resistst a margin x2 in the max currents and voltage,';
sentence2 = 'this will allow to 100% resist to transcient effects. ';
sentence3 = 'This gives ratings of: ';
sentence = [sentence1,sentence2,sentence3,'Vmax = ',num2str(2*Vg),'V and Imax =',num2str(2*Imax)];
disp(sentence);









