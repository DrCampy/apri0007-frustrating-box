;complementary single-phase PWM
;fully synchronous operation
;PWM period=5 microseconds
;PWM duty cycle= 50%
;clock = 4MHz

#include "config.inc"
#define CriticalAngleH
#define CriticalAngleL
#define VrefH
#define VrefL 
        
org 0x00
	nop
	goto start
org 0x04
	nop
	goto interrupt_routine
start:
      CALL initialisation
      goto  main_loop

interrupt_routine:
    
    ;if ADC go to adc interrupt (to do)
	
    ;if PWM go to pwm interrupt (to do)

initialisation:
;initialisation interrupts
    MOVLB 0x00
    MOVLW b'00000000'
    MOVWF PIR1          ;clear interrupts befor enable it
      
    MOVLB 0x01
    MOVLW b'11000000'
    MOVWF INTCON        ;enable global and perphiral interrupts
       
    MOVLW b'01000001'   ;enable ADC and Timer1 interrupts
    MOVWF PIE1          ;bit6 for ADC ,bit0 for Timer1
    IORWF PIE1,1        ;or operation to not affect on other bits
    
;configuration of clock 4MHz internal oscillator
    MOVLB 0x01 
    MOVLW b'01101010'
    MOVWF OSCCON
    MOVLW b'00000000'
    MOVWF OSCTUNE
    
;configure timer1 ON
    MOVLB 0x00
    MOVLW b'01000001'
    MOVWF TICON 
       
;initilize timer1 to trigger interrupt at 0.5ms 
    MOVLB 0x00
    BCF TICON,0
    MOVLB 0x00
    MOVLW 0xFF
    MOVWF TMR1H
    MOVLW 0xC1
    MOVWF TMR1L
    BSF   TICON,0
            
;configure PORTC        
    MOVLB 0x01            ;bank select TRISC  
    MOVLW b'00000000'     ;RC<7:0> as outputs ,RC0 is H-bridge-A
                          ;and RC1 is H-bridge-B
    MOVWF TRISC    
    CLRF LATC      
    
;configure PORTA
    MOVLB 0x01     ;bank select TRISA  
    MOVLW b'00000101'       ;RA<7:4> as outputs ,RA0 is Angle-sense, RA2 H-Bridge-EN
    MOVWF TRISA             ;(according to old Morgan's circuit)
    CLRF LATA    

;ADC Configuration 
        movlw	0x01
	movlw   b'01010101' ;12 bits result, enabled
	movwf	ADCON0 
	movlw   b'11000000' ; 2's Complement -  Fosc/4 - Vref = VDD/VSS
	movwf	ADCON1 
              
;configuration of PSMC
    BANKSEL PSMC1CON       ;bank 29 (Changing bank just after not usefull G:)          
                           ;selecting bank 29 where all of PMCx registers are
    movlw b'00010100'      ;set PR of the pwm to 20 because Fpic = 4MHz -> Tpic = 0.25 microsec  Fpwm = 2000kHz then Tpwm = 5 microsec  
					       ;period PWM = 5/0.25 = 20
    movwf PSMC1PRL
     
;set duty ycle
    CLRF PSMC1DCH         ; higher register of DC not used
    movlw b'00001010'     ; set to 90 because 50% of 20 = 10
    movwf PSMC1DCL      

;no phase offset
    CLRF PSMC1PHH
    CLRF PSMC1PHL    

;configuration of PSMC1CLK  
    MOVLW b'00000000'      ;use the 4 MHz clock
    MOVWF PSMC1CLK   

;output pin on A,normal polarity        
    MOVLW b'00000011'
    MOVWF PSMC1OEN         ;enable outputs A and B 
    MOVWF PSMC1STR0        ;steering the PWM signal to A and B
    CLRF  PSMC1POL

;set time base as source of all events
    BSF PSMC1PRS,P1PRST        
    BSF PSMC1PHS,P1PHST
    BSF PSMC1DCS,P1DCST

;clear dead band ?? (see if we should add deadband or maybe ask Mr Frebel)
    MOVLW b'00000000'    
    MOVWF PSMC1DBR
    MOVWF PSMC1DBF
;if we still don't use deadband gain 1 op G:
    ;CLRF PSMC1DBR
    ;CLRF PSMC1DBF

;enable PSMC in single phase complementary 
    MOVLW b'11000001'   
    MOVWF PSMC1CON
    
    return
          
;PSMC interrupt routine
interrupt_PSMC:
    BTFSS PIRI,TMR1F   ;check timer1 interrupt flag      
    goto end_if_timer1
     
    ;doesnt finish yet till obtain the values of critical angles
            
;ADC interrupt routine
    MOVLB 0x00           ;bank select
    BTFSS PIR1, 0       ;check if TMR1IF is '1'
    goto ADC_interrupt   ;if not goto next interrupt test 
              
;get next interrupt after 0.5ms 
    MOVLB 0x00
    BCF T1CON,0
    MOVLB 0x00
    MOVLW 0xFF
    MOVWF TMR1H
    MOVLW 0xC1
    MOVWF TMR1L
    BSF   T1CON,0   
              
    MOVLB 0x01
    BSF ADCON0,0        ;start conversion
    
    retfie ;because interrupt retfie
 
ADC_interrupt:

    MOVLB 0x00
    BTFSS PIR1,6             ;check if ADC interrupt flag = 1
    goto end_of_interrupt    ;if not,leave the interrupt
            
    if(ADRESH > CriricalAngleH){
        turn off motor
    }
    ;not finished -> wait for the values of critical angles
    
    retfie ;because interrupt retfie

main_loop:
    nop
    goto main_loop