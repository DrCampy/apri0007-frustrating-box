processor 16lf1789
#include	<xc.inc>  

;i2cbrg	    EQU 0x09		; Baud rate divider for 100kHz I2C (4MHz)
;i2cbrg	    EQU 0x09		; Baud rate divider for 400kHz I2C (16MHz)
i2cbrg	    EQU 0x13		; Baud rate divider for 400kHz I2C (32MHz)    
thresh	    EQU   15		; LED light-up threshold
i2c_flag    EQU    0		; I2C flag is bit 0 of reg flags1
fetch_flag  EQU    1		; New measurement flag is bit 1 of reg flags1
sensorAddr  EQU 0x42		; distance sensor address

; Variables for I2C  
PSECT i2cvar,space=SPACE_DATA,class=BANK4
i2c_state:
    DS 1			;1 byte

; Placed in the shared ram for faster access    
PSECT flags,space=SPACE_DATA,class=COMMON
flags1:
    DS 1			; 1 byte for the flags
last_dist:
    DS 1			; 1 byte for the last distance read.

PSECT bank0_var,space=1,class=BANK0
wait_sensor_init:
    DS 1
 
; Parameters for the configurations registers.
config FOSC=INTOSC
config WDTE=OFF
config PWRTE=ON
config MCLRE=ON
config CP=OFF
config CPD=OFF
config BOREN=ON
config CLKOUTEN=OFF
config IESO=OFF
config FCMEN=ON
config WRT=OFF
config PLLEN=OFF ; clock PLL contronned by SPLLEN in OSCCON
config STVREN=ON
config BORV=LO
config LPBOR=OFF
config LVP=OFF
   
;Send a byte through I2C and wait for completion.
i2cSendByteWait MACRO databyte
	banksel SSP1CON1	    
	movlw databyte		    
	movwf SSP1BUF
	banksel PIR1
	btfss SSP1IF
	goto $-1
	bcf SSP1IF
ENDM
    
;This is where the PIC will start executing the program (0x00)
PSECT code,abs
startup:
    org	0x00
    nop
    goto	start		    ;Jump to beginning of the code
    
;This is where the PIC will jump whenever an interrupt is triggered (0x04)
    org	0x04
    goto	interrupt_routines

;Beginning of the program
start:
    call	initialization
    goto	main_loop
  
 ;Initialization of the program
initialization:
    ; Configures the clock
    banksel	OSCCON
    movlw	11110000B	    ; SPLLEN = 1, IRCF = 1110, U, SCS = 00
    movwf	OSCCON		    ; configure oscillator (Fosc = 32MHz)
    movlw	00000000B	    
    movwf	OSCTUNE		    ; configure oscillator 
    
    ; Clears the variables
    clrf BANKMASK(flags1)
    clrf BANKMASK(last_dist)
        
    ; Configures timer 2 for 30 interrupts per second
    banksel T2CON
    movlw   01111111B		    ;0 - Postscale = 16 - Timer ON - Prescale = 64
    movwf   T2CON
    movlw   11111111B		    ; up to 256
    movwf   PR2
    clrf    TMR2	
    
    ; Wait 10 full timer cycle before initializing sensor
    movlw 10
    movwf BANKMASK(wait_sensor_init)
wait_before_timer:
    btfss TMR2IF
	goto $-1
    bcf TMR2IF
    decfsz BANKMASK(wait_sensor_init), F
        goto wait_before_timer
    
    ; Init i2c module
    call i2c_setup
    
    banksel TRISC		    ; Configures RC5 as output
    bcf RC5
    banksel PIE1		    ; Enables timer 2 interrupts
    bsf TMR2IE
    
    ; Enables global interrupts
    banksel INTCON
    bsf GIE			    ; Enables global interrupts
    bsf PEIE			    ; Enables peropheral interrupts
    return ;End of initialization
    
    interrupt_routines:		    ; This code manages the interrupts.
				    ; Interrupts should be tested in the order
				    ; of the most frequent to the less frequent.
				    ; First interrupt is boost.
    
    ;btfss TMR0IF		    ; Checks for the boost interrupt
    ;goto i2c_int
    ;
    ; boost code
    ;
    ;goto end_isr
i2c_int:    
    banksel PIR1		    
    btfss SSP1IF		    ; i2c interrupt
    goto timer2_int
    bcf SSP1IF			    ; Clears interrupt flag
    bsf BANKMASK(flags1), i2c_flag	    ; Sets the I2C flag
    goto end_isr
timer2_int:
    btfss TMR2IF		    ; Timer 2 interrupt
    goto end_isr
    bcf TMR2IF			    ; Clear interrupt flag
    bsf BANKMASK(flags1), fetch_flag    
end_isr:
    retfie			    ; Always use retfie to return from interrupts    
    
    
    ; Setup the i2c module and calls the sensor init routine
i2c_setup:
    ; Configures I2C
    ; SDA pin is RC3
    ; SCL pin is RC4
    banksel i2c_state
    clrf BANKMASK(i2c_state)

    ; Configures pins properly for SSP module
    banksel TRISC		    ; Data direction register
    bsf	TRISC3			    ; RC3 is input  
    bsf	TRISC4			    ; RC4 is input
    banksel ANSELC		    ; Analog function on pins
    bcf	ANSC3			    ; RC3 is digital
    bcf	ANSC4			    ; RC4 is digital

    banksel SSP1CON1		    ; Selects correct bank for SSP module
    movlw i2cbrg		    ; Sets I2C baud rate generator 
    movwf SSP1ADD		    ; SSPADD is BRG in I2C master mode
    movlw 00101000B		    ; SSPEN = 1 (enables module) SSPM = 1000 (I2C master mode)
    movwf SSP1CON1

    
    call sensor_init		    ; Initialize sensor before enabling interrupts
				    ; (function gets executed once, do not need
				    ; interrupts)
				    
    banksel PIR1		    ; Clears i2c interrupt flag
    bcf SSP1IF		    
    banksel PIE1		    ; Enables MSSP interrupts
    bsf SSP1IE
    return
    
    ; Initialize the sensor (send configuration bits and read address)
sensor_init:
    ;send Start - 0x42 (addr) - 0x00(reg) - 0b11001100(data) - 0x01(reg) - 
    ; 0b00000000(data) - 0x12(reg) - Stop
    
    ; Send start condition
    banksel SSP1CON1
    bsf SEN		    
    btfsc SEN			    ; wait for start condition to terminate
    goto $-1
    banksel PIR1		    ; Reset SSP interrupt flag
    bcf SSP1IF
    
    ;Send data bytes sensorAddr,0x00,11001100B,0x01,00000000B,0x12
    IRP dataByte,sensorAddr,0x00,11001100B,0x01,00000000B,0x12
	;; Send sensor data
	i2cSendByteWait dataByte
	banksel SSP1CON1
	btfsc ACKDT			    ;; Check if sensor acknowledged data
	goto sensor_init_err
    ENDM
    
    ; Send stop condition
    bsf PEN
    btfsc PEN			    ; wait for stop condition to terminate
    goto $-1
    bcf SSP1IF
    banksel PIR1		    ; Reset SSP interrupt flag
    bcf SSP1IF
sensor_init_err:    
    return

    ; Sets the flags to start a new measurement from the sensor.
    ; First test if the i2c is in correct state. If it is not no measurement
    ; will be fetched.
fetch_meas:
    banksel i2c_state
    movf BANKMASK(BANKMASK(i2c_state)), F   ; Move F to its location to test if it is 0
    btfsc ZERO
    bsf BANKMASK(flags1), i2c_flag
    bcf BANKMASK(flags1), fetch_flag
    return
    
    ; I2C execution routine. Have it's own state machine to keep track of the 
    ; next step to perform.
i2c_exec:
    banksel i2c_state
    movf BANKMASK(i2c_state), W
    brw
	goto i2c_send_start
	goto i2c_send_address
	goto i2c_read_mode    
	goto i2c_read_byte
	goto i2c_stop
	goto i2c_int_after_stop
    ; Send start condition
i2c_send_start:
    banksel SSP1CON1
    bsf SEN
    incf BANKMASK(i2c_state), F
    goto i2c_exec_end
    ; Send the address of the sensor on the bus
i2c_send_address:
    banksel SSP1CON1
    btfsc WCOL			    ; Test for potential collision
    goto i2c_exec_err		    
    btfsc SEN			    ; Check that start condition is over
    goto i2c_exec_err
    movlw sensorAddr+1		    ; Send address+1 for read mode
    movwf SSP1BUF
    incf BANKMASK(i2c_state), F	    ; Next state
    goto i2c_exec_end
    ; Check if sensor acked it's address and switch to receive mode
i2c_read_mode:
    banksel SSP1CON1
    btfsc WCOL			    ; Test for potential collision
    goto i2c_exec_err		    
    btfsc ACKSTAT		    ; Check that slave acked it's address
    goto i2c_exec_err
    bsf RCEN
    incf BANKMASK(i2c_state), F	    ; Next state
    goto i2c_exec_end
    ; Read the byte received from the sensor
i2c_read_byte:
    banksel SSP1CON1
    btfsc WCOL			    ; Test for potential collision
    goto i2c_exec_err
    btfsc SSPOV			    ; Test for potential receive overflow
    goto i2c_exec_err
    movf SSP1BUF,W		    ; Read distance
    movwf last_dist
    bsf ACKDT	    		    ; Send NACK
    bsf ACKEN	
    incf BANKMASK(i2c_state), F	    ; Next state
    goto i2c_exec_end
    ; Send stop condition
i2c_stop:
    banksel SSP1CON1
    bsf PEN			    ; Send stop
    incf BANKMASK(i2c_state), F
    goto i2c_exec_end
    ; One last interrupt is fired after the send bit have been sent
i2c_int_after_stop:
    clrf BANKMASK(i2c_state)
    goto i2c_exec_end
i2c_exec_err:
    clrf BANKMASK(i2c_state)	    ; Reset state machine
    movlw 00101000B		    ; Reset module state
    movwf SSP1CON1
i2c_exec_end:
    bcf BANKMASK(flags1), i2c_flag
    return
    
main_loop:
    ; We now test the possible flags to branch to the correct parts.
    btfsc BANKMASK(flags1), fetch_flag
    call fetch_meas
    
    btfsc BANKMASK(flags1), i2c_flag	    ; Test if the i2c flag have been set
    call i2c_exec
    
    ; Checks whether or not to light the LED
    banksel LATC
    movf BANKMASK(last_dist), W
    sublw thresh
    
    btfss CARRY
    bcf RC5
    btfsc CARRY
    bsf RC5
    
    CLRWDT			    ; Clears watchdog timer
    goto main_loop
    END startup