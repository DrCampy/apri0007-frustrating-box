;******************************************************************************;
;		    Code for the motor converter
;    
;	This code uses the following modules (and isr that must be goto'ed)
;	- ADC		with interrupt	    motor_ADC_interrupt
;	- Timer0	with interrupt	    motor_timer0_isr  
;	(- PSMC1		without interrupt)   
;	
;	This code provides the following functions
;	- motor_init	Must be called at init time. Initializes ADC, Timer0 and
;			PSMCX modules    

;	This code use the following ram spaces
;	- BANK1		15 bytes	
;	- COMMON	2 bytes
;******************************************************************************;    
 
    processor 16lf1789
    #include <xc.inc>
    
    MotorPIDSameSign	equ 0		; bit 0 of MotorFlags
    MotorComputeFlagRdy equ 0		; Flag to indicate that PID computation
					; is ready
    
    GLOBAL motor_init
    GLOBAL motor_ADC_isr
    GLOBAL motor_timer0_isr
    
    ; To be used by dist_table.s only
    GLOBAL AdMotorVrefH
    GLOBAL AdMotorVrefL
    
    leftShift MACRO regH, regL, times
	REPT times
	    lslf   BANKMASK(regL), F
	    rlf    BANKMASK(regH), F  
	ENDM
    ENDM

    ; Shifts regH:regL to the right 'times' times. Sign is NOT preserved.
    rightShift MACRO regH, regL, times
	REPT times
	    asrf   BANKMASK(regH), F  
	    rrf    BANKMASK(regL), F
	ENDM
    ENDM
		
; Memory organisation:
psect udata_bank1
    MotorFlags:		    DS 1
    AdcMotorH:		    DS 1    ; Value measured at the ADC
    AdcMotorL:		    DS 1
    AdMotorVrefH:	    DS 1    ; Value to be reached
    AdMotorVrefL:	    DS 1
    AdMotorErrH:	    DS 1    ; error at time t
    AdMotorErrL:	    DS 1
    AdMotorPropH:	    DS 1    ; Proportional term
    AdMotorPropL:	    DS 1    
    AdMotorIntH:	    DS 1    ; Integral term
    AdMotorIntL:	    DS 1
    AdMotorCumH:	    DS 1    ; cummulative integral term
    AdMotorCumL:	    DS 1
    AdMotorTmpSumH:	    DS 1    ; Term used when summing in PID
    AdMotorTmpSumL:	    DS 1
    
psect flags,space=SPACE_DATA,class=COMMON
    AdMotorPIDResH:	    DS 1    ; Sum of the 3 PID terms
    AdMotorPIDResL:	    DS 1
    
psect motor_code,class=CODE,space=SPACE_CODE,delta=2
motor_init:	
	;; Check if ADC config change is needed

	;; CONFIGURATION OF PSMC FOR MOTOR

	; Clear bank1 variables
	banksel MotorFlags
	IRP offset, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18
	    clrf BANKMASK(MotorFlags+offset)
	ENDM

	; Initialize Timer0 to trigger interrupt at 2 kHz
	banksel OPTION_REG
	movlw	11000111B
	andwf	OPTION_REG
	movlw	00000111B
	iorwf	OPTION_REG
	banksel TMR0
	movlw	101		; Interrupts at 200Hz (first 2 inc are skipped)
	movwf	TMR0
	bsf	TMR0IE		; Enables interrupts for timer 0
	
	;PSMC3 (25kHz PWM, 20cy deadband, Fosc/2 PSMC clock)
	banksel PSMC3CON 		
	movlw	0x02		    ; Period triggering time
	movwf	PSMC3PRH
	movlw	0x80		    ; set PR of the pwm to 25kHz
	movwf	PSMC3PRL
	movlw	0x01		    ; Duty Cycle at startup D=50%
	movwf	PSMC3DCH
	movlw	0x40
	movwf	PSMC3DCL
	clrf	PSMC3PHH	    ; no phase offset (PSMCxPH)
	clrf	PSMC3PHL                       
	;PSMC3 clk config:
	movlw	00010000B	    ; 00 - 1:2 - 00 - Fosc system clk (16MHz)
	movwf	PSMC3CLK   
	movlw	00000011B	    ; output pin on A and B,normal polarity 
	movwf	PSMC3OEN
	movwf	PSMC3STR0
	clrf	PSMC3POL
	bsf	P3PRST        
	bsf	P3PHST
	bsf	P3DCST
	movlw	20		    ; deadband set to 20cy
	movwf	PSMC3DBR
	movwf	PSMC3DBF    
	;enable PSMC in single phase complementary      
	movlw	11110001B	    ; Enabled; LD; DB=1; DB=1
	movwf	PSMC3CON
	banksel TRISE
	bcf	TRISE1		    ; pinB of PSMC3 corresponds to RE1
	bcf	TRISE2		    ; pinA of PSMC3 corresponds to RE2	
	
	; Configures RA1 (AN1) for analog input
	bsf	TRISA1	; RA1 input of ADC (=AN1)
	banksel ANSELA
	bsf	ANSA1
	banksel WPUA
	bcf	WPUA1
    return

motor_timer0_isr:	
	bcf	TMR0IF
	banksel TMR0
	movlw	101
	movwf	TMR0
	banksel ADCON0		; Start ADC conversion
	bsf	ADGO
    retfie    
    
motor_ADC_isr:
	bcf	ADIF		; Clear ADC interrupt f lag   
	banksel	ADRESL		; Stores the values from the ADC
	movf	ADRESL, W
	movwf	BANKMASK(AdcMotorL)
	movf	ADRESH, W
	movwf	BANKMASK(AdcMotorH)
	bsf	BANKMASK(MotorFlags),PIDFlag ; Rise flag for PID computation
    retfie
    
compute_motor_pid:	
	banksel MotorFlags	; Returns if flag not set
	btfss   BANKMASK(MotorFlags), MotorComputeFlagRdy
	    return
	; Computation of the error e = Vref - Vadc:
	bcf	BANKMASK(MotorFlags), MotorComputeFlagRdy
	movf    BANKMASK(AdcMotorL), W
	subwf   BANKMASK(AdMotorVrefL), W
	movwf   BANKMASK(AdMotorErrL)
	movf    BANKMASK(AdcMotorH), W   
	subwfb  BANKMASK(AdMotorVrefH), W
	movwf   BANKMASK(AdMotorErrH) 
	movlw	1111110B
	andwf	BANKMASK(AdMotorErrL),F
    
	; PID Calculation:
	movf    BANKMASK(AdMotorErrH),	W
	movwf   BANKMASK(AdMotorPropH)
	movf    BANKMASK(AdMotorErrL),	W
	movwf   BANKMASK(AdMotorPropL)
	rightShift AdMotorPropH, AdMotorPropL, 0
		    
	    
    no_overflow:
	;KI * err + prevCum (Kp = 25 -> Kp* ~= 2^5 (Adjusted to digital))
	movf	BANKMASK(AdMotorPropH), W ;start from Kp*err and shift 7 more times
	movwf	BANKMASK(AdMotorIntH)
	movf	BANKMASK(AdMotorPropL), W
	movwf	BANKMASK(AdMotorIntL)
	rightShift  AdMotorIntH, AdMotorIntL, 7
	
	; Multiply by 3
	movf	BANKMASK(AdMotorIntL), W
	movwf	BANKMASK(AdMotorTmpSumL)
	addwf	BANKMASK(AdMotorTmpSumL), F
	movf	BANKMASK(AdMotorIntH), W
	movwf	BANKMASK(AdMotorTmpSumH)
	addwfc	BANKMASK(AdMotorTmpSumH), F

	movf	BANKMASK(AdMotorIntL), W
	addwf	BANKMASK(AdMotorTmpSumL), F
	movf	BANKMASK(AdMotorIntH), W
	addwfc	BANKMASK(AdMotorTmpSumH), F
	
	movf	BANKMASK(AdMotorTmpSumL), W
	movwf	BANKMASK(AdMotorIntL)
	movf	BANKMASK(AdMotorTmpSumH), W
	movwf	BANKMASK(AdMotorIntH)

    add_int_cum:
	; Performs AdMotorInt + AdMotorCum and checks if it overflowed
	movf	BANKMASK(AdMotorCumL), W
	addwf	BANKMASK(AdMotorIntL), W
	movwf	BANKMASK(AdMotorTmpSumL)
	movf	BANKMASK(AdMotorCumH), W
	addwfc	BANKMASK(AdMotorIntH), W
	movwf	BANKMASK(AdMotorTmpSumH)
	movf	BANKMASK(AdMotorCumH), W    ; Check if operands were same sign
	xorwf	BANKMASK(AdMotorIntH), W
	btfsc	WREG, 7
	    goto    different_sign	    ; Guaranteed not to overflow
	; Same sign
	movf	BANKMASK(AdMotorTmpSumH), W
	xorwf	BANKMASK(AdMotorCumH), W
	btfsc	WREG, 7			    ; ans have opposite sign -> Overflwd
	    goto    skip_int_sum
	    
    different_sign:
	; Sum did not overflow. We store it in the correct register
	movf	BANKMASK(AdMotorTmpSumL), W
	movwf	BANKMASK(AdMotorCumL)
	movf	BANKMASK(AdMotorTmpSumH), W
	movwf	BANKMASK(AdMotorCumH)
  
    skip_int_sum:
	; Perform sum
	; Test if Prop and Cum are same sign (to further check for overflow)
	bcf	BANKMASK(MotorFlags), MotorPIDSameSign
	movf	BANKMASK(AdMotorPropH), W
	xorwf	BANKMASK(AdMotorCumH), W
	btfss	WREG, 7
	    bsf	    BANKMASK(MotorFlags), MotorPIDSameSign
	    
	movf	BANKMASK(AdMotorCumL), W    ; Sum L
	addwf	BANKMASK(AdMotorPropL), W
	movwf	BANKMASK(AdMotorPIDResL)
	
	movf	BANKMASK(AdMotorCumH), W    ; Sum H
	addwfc	BANKMASK(AdMotorPropH), W
	movwf	BANKMASK(AdMotorPIDResH)
	
	btfss	BANKMASK(MotorFlags), MotorPIDSameSign
	    goto    pid_sum_ok
	; Same sign
	movf	BANKMASK(AdMotorCumH), W
	xorwf	BANKMASK(AdMotorPIDResH), W
	btfss	WREG, 7			    ; if ans have != sign -> Overflwd
	    goto    pid_sum_ok
	
	btfss	BANKMASK(AdMotorPIDResH), 7 ; If we overflow in > 0 we have 
	    goto pid_send_zero_dc	    ; highly negative value and must
					    ; send 0 otherwise send max
    pid_send_max_dc:
	; If sum have overflowed DC is set to 100% then return
	banksel PSMC3DCH
	movlw	0x7F
	movwf	PSMC3DCH
	movlw	0xFF
	movwf	PSMC3DCL
	bsf	PSMC3LD
    return
	
    pid_sum_ok:
	; If PIDRes is < 0 the DC is set to 0% then return
	btfss	BANKMASK(AdMotorPIDResH), 7 ; if res < 0 we send 0 and are done.
	    goto send_Motor_dc
    pid_send_zero_dc:
	banksel PSMC3DCH
	clrf	PSMC3DCH
	clrf	PSMC3DCL
	bsf	PSMC3LD
    return
    send_Motor_dc:
	banksel PSMC3DCH
	movf	BANKMASK(AdMotorPIDResH), W
	movwf	PSMC3DCH
	movf	BANKMASK(AdMotorPIDResL), W
	movwf	PSMC3DCL
	bsf	PSMC3LD
    return

END	