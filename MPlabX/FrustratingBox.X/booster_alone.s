; *********************************************************** ;
;                  PID FOR BOOST CONVERTER                    ;
;             Reads ADC value, compares with ref              ;
;		  and generates a duty cycle                  ;
;						              ;
;            APRI0007 - Major Project in electronics          ;
;                                                             ;
; *********************************************************** ;
 
    processor 16lf1789
    #include <xc.inc>
    
    VrefH		equ 00001110B	; Vref = 3V on ADC 12-bits:
    VrefL		equ 10001011B	; Vref = 3723
    BoostPIDSameSign	equ 0		; bit number in 'Flags'
    PWMPeriodH		equ 00001000B	; Period = 2133 (independant of Fosc)
    PWMPeriodL		equ 01010101B
    PWMHighMaxH		equ 00000101B	; Max duty cycle is 80%
    PWMHighMaxL		equ 11010101B	; 0.8*2133 = 1706 or 1493 (0.7)
    TMR1ValH		equ 11111111B	; Value set in tmr1 for f = 5kHz
    TMR1ValL		equ 00111000B
		
		
    leftShift MACRO regH, regL, times
	REPT times
	    lslf   BANKMASK(regL), F
	    rlf    BANKMASK(regH), F  
	ENDM
    ENDM

    rightShift MACRO regH, regL, times
	REPT times
	    asrf   BANKMASK(regH), F  
	    rrf    BANKMASK(regL), F
	ENDM
    ENDM
    
    ; Parameters for the configurations registers.
    config FOSC = INTOSC
    config WDTE = OFF
    config PWRTE = ON
    config MCLRE = ON
    config CP = OFF
    config CPD = OFF
    config BOREN = ON
    config CLKOUTEN = OFF
    config IESO = OFF
    config FCMEN = ON
    config WRT = OFF
    config PLLEN = OFF
    config STVREN = ON
    config BORV = LO
    config LPBOR = OFF
    config LVP = OFF
    
     ; Memory organisation:
    psect udata_bank1 ; replace number of adress in bank1 by a label
	Flags:		    DS 1
	AdBoostVrefH:	    DS 1
	AdBoostVrefL:	    DS 1
	AdBoostErrH:	    DS 1    ; error at time t
	AdBoostErrL:	    DS 1
	AdBoostPrevErrH:    DS 1    ; error at time t-1
	AdBoostPrevErrL:    DS 1
	AdBoostPropH:	    DS 1    ; Proportional term
	AdBoostPropL:	    DS 1
	AdBoostIntH:	    DS 1    ; Integral term = proportional term
	AdBoostIntL:	    DS 1
	AdBoostCumH:	    DS 1    ; Integrational term
	AdBoostCumL:	    DS 1
	AdBoostDiffH:	    DS 1    ; Derivative term
	AdBoostDiffL:	    DS 1
	AdBoostTmpSumH:	    DS 1    ; Term used when summing in boost isr
	AdBoostTmpSumL:	    DS 1
    
    psect flags,space=SPACE_DATA,class=COMMON
	AdBoostPIDResH:	    DS 1    ; Sum of the 3 PID terms
	AdBoostPIDResL:	    DS 1
    
; That is where the MCU will start executing the program (0x00)
psect code,abs,ovrld
    startup: 
    org	    0x0
    nop
    goto    start		; jump to the beginning of the code
	
;Give the address of the interrupt routine to the interrupt vector
    org	    0x04 
    goto    interrupt

;BEGINNING OF THE PROGRAM
start:
    call    initialisation	; initialisation routine configuring the MCU
    goto    main_loop		; main loop

;INITIALISATION
initialisation:	
    ; Configuration of clock - freq = 32MHZ - internal oscillator block 
	banksel	OSCCON
	movlw   11110000B	; SPLLEN = 1, IRCF = 1110, U, SCS = 00
	movwf   OSCCON		; configure oscillator (Fosc = 32MHz)	
	banksel OSCTUNE
	movlw   00000000B	    
	movwf   OSCTUNE   

	; Configuration of ADC (reads on pin RA0)
	banksel TRISA
	bsf     TRISA0		; Input
	banksel ANSELA
	bsf     ANSA0
	banksel WPUA
	bcf     WPUA0
	banksel ADCON0
	movlw   00000001B	; 12 bits result, AN0, ~GO, module enabled
	movwf   ADCON0 
	movlw   10100000B	; 2's Complement -  Fosc/32 - 0 - Vref = VDD/VSS
	movwf   ADCON1
	movlw   00001111B	; no auto-conv
	movwf   ADCON2

	; Configuration of PWM signal:
	; Set period to 33.33s with a 64MHz clk:
	banksel PSMC1CON
	movlw   PWMPeriodH	; Period
	movwf   PSMC1PRH
	movlw   PWMPeriodL	; Period
	movwf   PSMC1PRL
	; Initialize with D = 0% [rising edge = falling edge]:
	clrf    PSMC1DCH
	clrf    PSMC1DCL 
	clrf    PSMC1PHH	; No phase offset:  
	clrf    PSMC1PHL
	movlw   00000001B	; Clock frequency = 64MHz: 
	movwf   PSMC1CLK
	; Set output on A with normal polarity:
	bsf	P1STRA
	bcf	P1POLA
	bsf	P1OEA
	; Set time base event for all events:
	bsf	P1PRST
	bsf	P1PHST	
	bsf	P1DCST
	; Enable the PWM and the output pin:
	movlw   11000000B	; No deadbands and single PWM waveform
	movwf   PSMC1CON
	banksel TRISC
	bcf	TRISC, 0	; pinA of PSMC1 corresponds to RC0

	; Storing reference in memory
	banksel Flags
	movlw   VrefH
	movwf   BANKMASK(AdBoostVrefH)	
	movlw   VrefL
	movwf   BANKMASK(AdBoostVrefL)

	; Setting Boost PID registers to 0x00
	IRP offset, 0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
	    clrf BANKMASK(Flags+offset)
	ENDM
	
	; Enable interrupts:
	banksel PIR1	
	clrf    PIR1		; clear interrupt flags before enabling interupts.
	clrf    INTCON		; Ensures interrupts are disabled

	banksel PIE1
	movlw   01000001B
	iorwf   PIE1, F		; OR operation to not affect the other bits.
	movlw   11000000B	;enable global and peripheral interupts.
	movwf   INTCON

	; Initialize Timer1 to trigger interrupt at 200us [f=5kHz]
	banksel T1CON
	movlw   00110000B	; TMR1CS=00(Fosc/4), T1CKPS=11(1:8), T1OSCEN=0 
	movwf   T1CON		; ~T1SYNC=0, 0, TMR1ON=0 (timer off)
	movlw   11111111B
	movwf   TMR1H
	;movlw   00111000B	; 5kHz
	movlw	01111011B	; 7.5kHz
	movwf   TMR1L
	bsf	TMR1ON
    return

;INTERRUPT ROUTINE
interrupt:	
	banksel PIR1
	btfss   TMR1IF		; Check the source of interrupt
	    goto	boost_ADC_interrupt
	bcf	TMR1IF
	banksel T1CON		; Reset timer 1
	bcf	TMR1ON
	movlw	TMR1ValH
	movwf	TMR1H
	movlw	TMR1ValL
	movwf	TMR1L
	bsf	TMR1ON
	banksel ADCON0		; Start ADC conversion
	bsf	ADGO
	goto	end_of_interrupt
    
boost_ADC_interrupt:
	banksel PIR1
	btfss	ADIF		; Leave interrupt routine if ADC flag is not set
	    goto end_of_interrupt
	bcf ADIF ; Clear ADC interrupt flag
    
	; Computation of the error e = Vref - Vadc:
	banksel ADRESL; Same bank as our variables
	movf    ADRESL, W
	subwf   BANKMASK(AdBoostVrefL), W
	movwf   BANKMASK(AdBoostErrL)
	movf    ADRESH, W   
	subwfb  BANKMASK(AdBoostVrefH), W
	movwf   BANKMASK(AdBoostErrH)  
    
	; PID Calculation:
	; Kp * err (Kp = 0.015625 -> Kp* ~= 2^-5 (Adjusted to digital) )
	movf    BANKMASK(AdBoostErrH),	W
	movwf   BANKMASK(AdBoostPropH)
	movf    BANKMASK(AdBoostErrL),	W
	movwf   BANKMASK(AdBoostPropL)
	rightShift AdBoostPropH, AdBoostPropL, 5    
	
	; Ki*err (Ki = Kp = 0.015625 -> Kp* ~= 2^-6 once digital and tuned)
	movf	BANKMASK(AdBoostPropH), W
	movwf	BANKMASK(AdBoostIntH)
	movf	BANKMASK(AdBoostPropL), W
	movwf	BANKMASK(AdBoostIntL)
	rightShift AdBoostIntH, AdBoostIntL, 1
	
	; Performs AdBoostInt + AdBoostCum and checks if it overflowed
	movf	BANKMASK(AdBoostCumL), W
	addwf	BANKMASK(AdBoostIntL), W
	movwf	BANKMASK(AdBoostTmpSumL)
	movf	BANKMASK(AdBoostCumH), W
	addwfc	BANKMASK(AdBoostIntH), W
	movwf	BANKMASK(AdBoostTmpSumH)	    
	movf	BANKMASK(AdBoostCumH), W    ; Check if operands were same sign
	xorwf	BANKMASK(AdBoostIntH), W
	btfsc	WREG, 7
	    goto    different_sign	    ; Guaranteed not to overflow
	; Same sign
	movf	BANKMASK(AdBoostTmpSumH), W
	xorwf	BANKMASK(AdBoostCumH), W
	btfsc	WREG, 7			    ; ans have opposite sign -> Overflwd
	    goto    skip_int_sum
	    
different_sign:
	; Sum did not overflow. We store it in the correct register
	movf	BANKMASK(AdBoostTmpSumL), W
	movwf	BANKMASK(AdBoostCumL)
	movf	BANKMASK(AdBoostTmpSumH), W
	movwf	BANKMASK(AdBoostCumH)
  
skip_int_sum:	    
boost_derivative_term:
	;Kd = 0.125 -> Kd* ~= 2^-2 (Adjusted to digital) 
	; Calculation of DeltaE[k] = e[k]- e[k-1]:
	movf	BANKMASK(AdBoostPrevErrL), W
	subwf	BANKMASK(AdBoostErrL), W
	movwf	BANKMASK(AdBoostDiffL)
	movf	BANKMASK(AdBoostPrevErrH), W
	subwfb	BANKMASK(AdBoostErrH), W
	movwf	BANKMASK(AdBoostDiffH)
	rightShift AdBoostDiffH, AdBoostDiffL, 2
	
	; Move error in prevError location for next computation
	movf	BANKMASK(AdBoostErrH), W
	movwf   BANKMASK(AdBoostPrevErrH)
	movf    BANKMASK(AdBoostErrL), W 
	movwf   BANKMASK(AdBoostPrevErrL)
	
	; Perform sum
	movf	BANKMASK(AdBoostPropL), W   ; Prop + Diff
	addwf	BANKMASK(AdBoostDiffL), W
	movwf	BANKMASK(AdBoostPIDResL)    
	movf	BANKMASK(AdBoostPropH), W
	addwfc	BANKMASK(AdBoostDiffH), W
	movwf	BANKMASK(AdBoostPIDResH)
	; Test if Res and Cum are same sign (to further check for overflow)
	bcf	BANKMASK(Flags), BoostPIDSameSign
	movf	BANKMASK(AdBoostPIDResH), W
	xorwf	BANKMASK(AdBoostCumH), W
	btfss	WREG, 7
	    bsf	    BANKMASK(Flags), BoostPIDSameSign
	movf	BANKMASK(AdBoostCumL), W
	addwf	BANKMASK(AdBoostPIDResL), F
	movf	BANKMASK(AdBoostCumH), W
	addwfc	BANKMASK(AdBoostPIDResH), F
	btfss	BANKMASK(Flags), BoostPIDSameSign
	    goto    pid_sum_ok

	; Same sign
	movf	BANKMASK(AdBoostCumH), W
	xorwf	BANKMASK(AdBoostPIDResH), W
	btfss	WREG, 7			    ; if ans have != sign -> Overflwd
	    goto    pid_sum_ok
	
	btfss	BANKMASK(AdBoostPIDResH), 7
	    goto pid_send_zero_dc
pid_send_max_dc:
	; If sum have overflowed DC is set to 80% then return
	banksel PSMC1DCH
	movlw	PWMHighMaxH
	movwf	PSMC1DCH
	movlw	PWMHighMaxL
	movwf	PSMC1DCL
	bsf	PSMC1LD
	retfie
pid_sum_ok:
	; If PIDRes is < 0 the DC is set to 0% then return
	btfss	BANKMASK(AdBoostPIDResH), 7 ; if res < 0 we send 0 and are done.
	    goto send_boost_dc
pid_send_zero_dc:
	banksel PSMC1DCH
	clrf	PSMC1DCH
	clrf	PSMC1DCL
	bsf	PSMC1LD
	retfie
send_boost_dc:
	; check if PIDRes > Max duty cycle
	movf BANKMASK(AdBoostPIDResL), W
	sublw PWMHighMaxL
	movf BANKMASK(AdBoostPIDResH), W
	btfss CARRY
	    incf	WREG, W
	sublw PWMHighMaxH
	btfss CARRY
	    goto	pid_send_max_dc
	banksel PSMC1DCH
	movf	BANKMASK(AdBoostPIDResH), W
	movwf	PSMC1DCH
	movf	BANKMASK(AdBoostPIDResL), W
	movwf	PSMC1DCL
	bsf	PSMC1LD
end_of_interrupt:
    retfie
    	
;MAIN LOOP
main_loop:
    nop
    clrwdt		    ;clear watchDogTimer
    goto main_loop
END