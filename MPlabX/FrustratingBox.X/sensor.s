;******************************************************************************;
;		    Code for the i2c distance sensor   
;    
;	This code uses the following modules (and isr that must be goto'ed)
;	- SSP1		with interrupt	    sensor_ssp1_isr
;	- Timer2	with interrupt	    sensor_timer2_isr    
;	
;	This code provides the following functions
;	- sensor_init	Must be called at init time. Initializes Timer2 SSP1 and 
;			the i2c distance sensor
;	- sensor_i2c_exec   
;			Must be called regularly to execute the pending i2c
;			operations. Ifd no operation is pending the code returns
;			in 3 instruction cycles.
;	- sensor_fetch_measurement
;			This function fetches a measurement if a request is
;			pending	
;    
;	This code use the following ram spaces
;	- BANK0		1 byte
;	- BANK4		1 byte	
;	- COMMON	2 bytes
;	
;	The following data/variables are made available
;	- last_dist	1 byte	contains the last distance (in cm) measured by
;				the sensor. Updated after a call to 
;				sensor_fetch_measurement if a request was
;				pending. (COMMON Ram)
;******************************************************************************;    
    
processor 16lf1789
#include	<xc.inc>  

i2cbrg		EQU 0x13	; Baud rate divider for 400kHz I2C (32MHz)    
i2c_rdy_flag    EQU    0	; I2C flag is bit 0 of reg i2c_flags
fetch_flag	EQU    1	; New measurement flag is bit 1 of reg i2c_flags
sensorAddr	EQU 0x42	; distance sensor address

GLOBAL sensor_init
GLOBAL sensor_timer2_isr
GLOBAL sensor_ssp1_isr
GLOBAL sensor_i2c_exec
GLOBAL sensor_fetch_measurement	
GLOBAL last_dist	
	
; Variables for I2C  
PSECT udata_bank4
i2c_state:  DS 1		; 1 byte for i2c fsm

; Placed in the shared ram for faster access    
PSECT udata_shr
i2c_flags:  DS 1		; 1 byte for the flags
last_dist:  DS 1		; 1 byte for the last distance read.

PSECT udata_bank0
wait_sensor_init:   DS 1	; Number of full timer cycles to wait for sensor
				; to start
   
				
;Send a byte through I2C and wait for completion.
i2cSendByteWait MACRO databyte
	banksel SSP1CON1	    
	movlw databyte		    
	movwf SSP1BUF
	banksel PIR1
	btfss SSP1IF
	    goto $-1
	bcf SSP1IF
ENDM

PSECT sensor_code,class=CODE,space=SPACE_CODE,delta=2
sensor_init:   
    ; Clears the variables
    clrf BANKMASK(i2c_flags)
    clrf BANKMASK(last_dist)
    banksel i2c_state
    clrf BANKMASK(i2c_state)
        
    ; Configures timer 2 for 30 interrupts per second
    banksel T2CON
    movlw   01111111B		    ; Postscale = 16 - Timer ON - Prescale = 64
    movwf   T2CON
    movlw   11111111B		    ; up to 256
    movwf   PR2
    clrf    TMR2
    
    ; Wait 10 full timer cycle before initializing sensor
    movlw 10
    movwf BANKMASK(wait_sensor_init)
wait_before_timer:
    btfss TMR2IF
	goto $-1
    bcf TMR2IF
    decfsz BANKMASK(wait_sensor_init), F
	goto wait_before_timer
    
    ; Init i2c module
    call i2c_setup
    
    banksel TRISC		    ; Configures RC5 as output
    bcf RC5
    banksel PIE1		    ; Enables timer 2 interrupts
    bsf TMR2IE	
    
return ;End of sensor_init
    
sensor_ssp1_isr:    
    bcf SSP1IF			    ; Clears interrupt flag
    bsf BANKMASK(i2c_flags), i2c_rdy_flag	    ; Sets the I2C flag
    retfie
    
sensor_timer2_isr:
    bcf TMR2IF			    ; Clear interrupt flag
    bsf BANKMASK(i2c_flags), fetch_flag    
    retfie
    
    ; Setup the i2c module and calls the sensor init routine
i2c_setup:
    ; Configures I2C
    ; SDA pin is RC3
    ; SCL pin is RC4

    ; Configures pins properly for SSP module
    banksel TRISC		    ; Data direction register
    bsf	TRISC3			    ; RC3 is input  
    bsf	TRISC4			    ; RC4 is input
    banksel ANSELC		    ; Analog function on pins
    bcf	ANSC3			    ; RC3 is digital
    bcf	ANSC4			    ; RC4 is digital

    banksel SSP1CON1		    ; Selects correct bank for SSP module
    movlw i2cbrg		    ; Sets I2C baud rate generator 
    movwf SSP1ADD		    ; SSPADD is BRG in I2C master mode
    movlw 00101000B		    ; SSPEN = 1 (enables module) SSPM = 1000 (I2C master mode)
    movwf SSP1CON1

    
    call hawkseye_init		    ; Initialize sensor before enabling interrupts
				    ; (function gets executed once, do not need
				    ; interrupts)
				    
    banksel PIR1		    ; Clears i2c interrupt flag
    bcf SSP1IF		    
    banksel PIE1		    ; Enables MSSP interrupts
    bsf SSP1IE
    return
    
    ; Initialize the sensor (send configuration bits and read address)
hawkseye_init:
	;send Start - 0x42 (addr) - 0x00(reg) - 0b11001100(data) - 0x01(reg) - 
	; 0b00000000(data) - 0x12(reg) - Stop

	; Send start condition
	banksel SSP1CON1
	bsf SEN		    
	btfsc SEN			    ; wait for start condition to terminate
	goto $-1
	banksel PIR1		    ; Reset SSP interrupt flag
	bcf SSP1IF

	;Send data bytes sensorAddr,0x00,11001100B,0x01,00000000B,0x12
	IRP dataByte,sensorAddr,0x00,11001100B,0x01,00000000B,0x12
	    i2cSendByteWait dataByte	    ;; Send sensor data
	    banksel SSP1CON1
	    btfsc ACKDT			    ;; Check if sensor acknowledged data
		goto hawkseye_init_err
	ENDM

	; Send stop condition
	bsf PEN
	btfsc PEN			    ; wait for stop condition to terminate
	goto $-1
	;bcf SSP1IF
	banksel PIR1		    ; Reset SSP interrupt flag
	bcf SSP1IF
hawkseye_init_err:    
    return

    ; Sets the flags to start a new measurement from the sensor.
    ; First test if the i2c is in correct state. If it is not no measurement
    ; will be fetched.
sensor_fetch_measurement:
	btfss BANKMASK(i2c_flags), fetch_flag
	    return				; If no request pending, return
	banksel i2c_state
	movf BANKMASK(i2c_state), F   ; Check if i2c_state is 0
	btfsc ZERO				; If it is not we do not start
	    bsf BANKMASK(i2c_flags), i2c_rdy_flag		; a measurement this time.
	bcf BANKMASK(i2c_flags), fetch_flag
    return
    
    ; I2C execution routine. Have it's own state machine to keep track of the 
    ; next step to perform.
sensor_i2c_exec:
	btfss	BANKMASK(i2c_flags), i2c_rdy_flag
	    return			    ; If flag is not set we return
	banksel i2c_state
	movf BANKMASK(i2c_state), W
	brw
	    goto i2c_send_start
	    goto i2c_send_address
	    goto i2c_read_mode    
	    goto i2c_read_byte
	    goto i2c_stop
	    goto i2c_int_after_stop
	; Send start condition
i2c_send_start:
	banksel SSP1CON1
	bsf SEN
	incf BANKMASK(i2c_state), F
	goto i2c_exec_end
	; Send the address of the sensor on the bus
i2c_send_address:
	banksel SSP1CON1
	btfsc WCOL			    ; Test for potential collision
	    goto i2c_exec_err		    
	btfsc SEN			    ; Check that start condition is over
	    goto i2c_exec_err
	movlw sensorAddr+1		    ; Send address+1 for read mode
	movwf SSP1BUF
	incf BANKMASK(i2c_state), F	    ; Next state
	goto i2c_exec_end
	; Check if sensor acked it's address and switch to receive mode
i2c_read_mode:
	banksel SSP1CON1
	btfsc WCOL			    ; Test for potential collision
	goto i2c_exec_err		    
	btfsc ACKSTAT		    ; Check that slave acked it's address
	goto i2c_exec_err
	bsf RCEN
	incf BANKMASK(i2c_state), F	    ; Next state
	goto i2c_exec_end
	; Read the byte received from the sensor
i2c_read_byte:
	banksel SSP1CON1
	btfsc WCOL			    ; Test for potential collision
	goto i2c_exec_err
	btfsc SSPOV			    ; Test for potential receive overflow
	goto i2c_exec_err
	movf SSP1BUF,W		    ; Read distance
	movwf BANKMASK(last_dist)
	bsf ACKDT	    		    ; Send NACK
	bsf ACKEN	
	incf BANKMASK(i2c_state), F	    ; Next state
	goto i2c_exec_end
	; Send stop condition
i2c_stop:
	banksel SSP1CON1
	bsf PEN			    ; Send stop
	incf BANKMASK(i2c_state), F
	goto i2c_exec_end
	; One last interrupt is fired after the send bit have been sent
i2c_int_after_stop:
	clrf BANKMASK(i2c_state)
	goto i2c_exec_end
i2c_exec_err:
	clrf BANKMASK(i2c_state)	    ; Reset state machine
	movlw 00101000B		    ; Reset module state
	movwf SSP1CON1
i2c_exec_end:
	bcf BANKMASK(i2c_flags), i2c_rdy_flag
    return
END