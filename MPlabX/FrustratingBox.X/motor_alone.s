processor 16lf1789
#include <xc.inc>

;VrefH		    EQU 00000100B 
;VrefL		    EQU 11011000B		    
VrefH		    EQU 00000110B 
VrefL		    EQU 11001001B

MotorPIDSameSign    EQU 0	    ; bit 0 of flags
MotorComputeFlagRdy EQU 1	    ; bit 1 of flags


; Shifts regH:regL to the left 'times' times. Sign is preserved
; Might overflow
leftShift MACRO regH, regL, times
    REPT times
	lslf   BANKMASK(regL), F
	rlf    BANKMASK(regH), F  
    ENDM
ENDM

; Shifts regH:regL to the right 'times' times. Sign is NOT preserved.
rightShift MACRO	regH, regL, times
    REPT times
	asrf   BANKMASK(regH), F  
	rrf    BANKMASK(regL), F
    ENDM
ENDM
	    	    
; Parameters for the configurations registers.
config FOSC=INTOSC
config WDTE=OFF
config PWRTE=ON
config MCLRE=ON
config CP=OFF
config CPD=OFF
config BOREN=ON
config CLKOUTEN=OFF
config IESO=OFF
config FCMEN=ON
config WRT=OFF
config PLLEN=OFF ; clock PLL controlled by SPLLEN in OSCCON
config STVREN=ON
config BORV=LO
config LPBOR=OFF
config LVP=OFF	    
	    
	    
psect udata_bank1
    MotorFlags:		    DS 1
    AdcMotorH:		    DS 1    ; Value measured at the ADC
    AdcMotorL:		    DS 1
    AdMotorVrefH:	    DS 1    ; Value to be reached
    AdMotorVrefL:	    DS 1
    AdMotorErrH:	    DS 1    ; error at time t
    AdMotorErrL:	    DS 1
    AdMotorPropH:	    DS 1    ; Proportional term
    AdMotorPropL:	    DS 1    
    AdMotorIntH:	    DS 1    ; Integral term
    AdMotorIntL:	    DS 1
    AdMotorCumH:	    DS 1    ; cummulative integral term
    AdMotorCumL:	    DS 1
    AdMotorTmpSumH:	    DS 1    ; Term used when summing in PID
    AdMotorTmpSumL:	    DS 1
    
    
psect flags,space=SPACE_DATA,class=COMMON
    AdMotorPIDResH:	    DS 1    ; Sum of the 3 PID terms
    AdMotorPIDResL:	    DS 1

; That is where the MCU will start executing the program (0x00)
psect code,abs,ovrld
startup: 
    org	    0x0
    nop
    goto    start		; jump to the beginning of the code
	
;Give the address of the interrupt routine to the interrupt vector
    org	    0x04 
    goto    interrupt
	
start:
	call	initialisation
	goto	main_loop
	
initialisation:
    ; Configuration of clock - freq = 32MHZ - internal oscillator block 
	banksel OSCCON
	movlw	11110000B
	movwf	OSCCON	
	banksel OSCTUNE
	movlw	00000000B	    
	movwf	OSCTUNE
	
	; Clear bank1 variables
	banksel MotorFlags
	IRP offset, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14
	    clrf BANKMASK(MotorFlags+offset)
	ENDM
	
	; Initializes VRef (at 1V)
	movlw	VrefH
	movwf	BANKMASK(AdMotorVrefH)
	movlw	VrefL
	movwf	BANKMASK(AdMotorVrefL)
	  
	banksel OPTION_REG
	movlw	11000111B
	andwf	OPTION_REG
	movlw	00000111B
	iorwf	OPTION_REG
	banksel TMR0
	movlw	101		; 200Hz sampling frequency
	movwf	TMR0		
	bsf	TMR0IE		; Enables interrupts for timer 0
	
	;PSMC3 (25kHz PWM, 20cy deadband, Fosc/2 PSMC clock)
	banksel PSMC3CON 
	movlw	0x02		    ; Period triggering time
	movwf	PSMC3PRH
	movlw	0x80		    ; set PR of the pwm to 25kHz
	movwf	PSMC3PRL
	movlw	0x01		    ; Duty Cycle at startup D=50%
	movwf	PSMC3DCH
	movlw	0x40
	movwf	PSMC3DCL
	clrf	PSMC3PHH	    ; no phase offset (PSMCxPH)
	clrf	PSMC3PHL                       
	;PSMC3 clk config:
	movlw	00010000B	    ; 00 - 1:2 - 00 - Fosc system clk (16MHz)
	movwf	PSMC3CLK   
	movlw	00000011B	    ; output pin on A and B,normal polarity 
	movwf	PSMC3OEN
	movwf	PSMC3STR0
	clrf	PSMC3POL
	bsf	P3PRST        
	bsf	P3PHST
	bsf	P3DCST
	movlw	20		    ; deadband set to 20cy
	movwf	PSMC3DBR
	movwf	PSMC3DBF    
	;enable PSMC in single phase complementary      
	movlw	11110001B	    ; Enabled; LD; DB=1; DB=1
	movwf	PSMC3CON
	banksel TRISE
	bcf	TRISE1		    ; pinB of PSMC3 corresponds to RE1
	bcf	TRISE2		    ; pinA of PSMC3 corresponds to RE2	
    
	; Configures ADC
	bsf	TRISA1	; RA1 input of ADC (=AN1)
	banksel ANSELA
	bsf	ANSA1
	banksel WPUA
	bcf	WPUA1
	banksel ADCON0
	movlw   00000101B ; enable ADC and AN1 selected
	movwf	ADCON0 
	movlw   10100000B ; 2's Complement -  Fosc/32 - 0  - Vref = VDD/VSS
	movwf	ADCON1
	movlw	00001111B   ; no auto-conv
	movwf	ADCON2	
	banksel PIE1
	bsf	ADIE
	
	banksel PIR1	    ; Enable global interrupts
	clrf	PIR1
	bsf	GIE
    return

	
interrupt:
    banksel PIR1
    btfsc   ADIF
	goto adc_isr
    btfsc   TMR0IF
	goto timer0_isr

    retfie			; if we arrive here there is an isr not managed!

timer0_isr:
	bcf	TMR0IF		; reset the timer
	banksel TMR0
	movlw   101		; 200 Hz
	movwf   TMR0		
	banksel ADCON0		; Start ADC conversion
	bsf ADGO	
    retfie
    
adc_isr:
	bcf	ADIF		; Clear ADC interrupt f lag   
	banksel	ADRESL		; Stores the values from the ADC
	movf	ADRESL, W
	movwf	BANKMASK(AdcMotorL)
	movf	ADRESH, W
	movwf	BANKMASK(AdcMotorH)
	bsf	BANKMASK(MotorFlags),MotorComputeFlagRdy ; Rise flag for PID computation
    retfie
    
compute_pid:	
	banksel MotorFlags	; Returns if flag not set
	btfss   BANKMASK(MotorFlags), MotorComputeFlagRdy
	    return
	; Computation of the error e = Vref - Vadc:
	bcf	BANKMASK(MotorFlags), MotorComputeFlagRdy
	movf    BANKMASK(AdcMotorL), W
	subwf   BANKMASK(AdMotorVrefL), W
	movwf   BANKMASK(AdMotorErrL)
	movf    BANKMASK(AdcMotorH), W   
	subwfb  BANKMASK(AdMotorVrefH), W
	movwf   BANKMASK(AdMotorErrH) 
	movlw	1111110B
	andwf	BANKMASK(AdMotorErrL),F
    
	; PID Calculation:
	movf    BANKMASK(AdMotorErrH),	W
	movwf   BANKMASK(AdMotorPropH)
	movf    BANKMASK(AdMotorErrL),	W
	movwf   BANKMASK(AdMotorPropL)
	rightShift AdMotorPropH, AdMotorPropL, 0
		    
	    
    no_overflow:
	;KI * err + prevCum (Kp = 25 -> Kp* ~= 2^5 (Adjusted to digital))
	movf	BANKMASK(AdMotorPropH), W ;start from Kp*err and shift 7 more times
	movwf	BANKMASK(AdMotorIntH)
	movf	BANKMASK(AdMotorPropL), W
	movwf	BANKMASK(AdMotorIntL)
	rightShift  AdMotorIntH, AdMotorIntL, 7
	
	; Multiply by 3
	movf	BANKMASK(AdMotorIntL), W
	movwf	BANKMASK(AdMotorTmpSumL)
	addwf	BANKMASK(AdMotorTmpSumL), F
	movf	BANKMASK(AdMotorIntH), W
	movwf	BANKMASK(AdMotorTmpSumH)
	addwfc	BANKMASK(AdMotorTmpSumH), F

	movf	BANKMASK(AdMotorIntL), W
	addwf	BANKMASK(AdMotorTmpSumL), F
	movf	BANKMASK(AdMotorIntH), W
	addwfc	BANKMASK(AdMotorTmpSumH), F
	
	movf	BANKMASK(AdMotorTmpSumL), W
	movwf	BANKMASK(AdMotorIntL)
	movf	BANKMASK(AdMotorTmpSumH), W
	movwf	BANKMASK(AdMotorIntH)

    add_int_cum:
	; Performs AdMotorInt + AdMotorCum and checks if it overflowed
	movf	BANKMASK(AdMotorCumL), W
	addwf	BANKMASK(AdMotorIntL), W
	movwf	BANKMASK(AdMotorTmpSumL)
	movf	BANKMASK(AdMotorCumH), W
	addwfc	BANKMASK(AdMotorIntH), W
	movwf	BANKMASK(AdMotorTmpSumH)
	movf	BANKMASK(AdMotorCumH), W    ; Check if operands were same sign
	xorwf	BANKMASK(AdMotorIntH), W
	btfsc	WREG, 7
	    goto    different_sign	    ; Guaranteed not to overflow
	; Same sign
	movf	BANKMASK(AdMotorTmpSumH), W
	xorwf	BANKMASK(AdMotorCumH), W
	btfsc	WREG, 7			    ; ans have opposite sign -> Overflwd
	    goto    skip_int_sum
	    
    different_sign:
	; Sum did not overflow. We store it in the correct register
	movf	BANKMASK(AdMotorTmpSumL), W
	movwf	BANKMASK(AdMotorCumL)
	movf	BANKMASK(AdMotorTmpSumH), W
	movwf	BANKMASK(AdMotorCumH)
  
    skip_int_sum:
	; Perform sum
	; Test if Prop and Cum are same sign (to further check for overflow)
	bcf	BANKMASK(MotorFlags), MotorPIDSameSign
	movf	BANKMASK(AdMotorPropH), W
	xorwf	BANKMASK(AdMotorCumH), W
	btfss	WREG, 7
	    bsf	    BANKMASK(MotorFlags), MotorPIDSameSign
	    
	movf	BANKMASK(AdMotorCumL), W    ; Sum L
	addwf	BANKMASK(AdMotorPropL), W
	movwf	BANKMASK(AdMotorPIDResL)
	
	movf	BANKMASK(AdMotorCumH), W    ; Sum H
	addwfc	BANKMASK(AdMotorPropH), W
	movwf	BANKMASK(AdMotorPIDResH)
	
	btfss	BANKMASK(MotorFlags), MotorPIDSameSign
	    goto    pid_sum_ok
	; Same sign
	movf	BANKMASK(AdMotorCumH), W
	xorwf	BANKMASK(AdMotorPIDResH), W
	btfss	WREG, 7			    ; if ans have != sign -> Overflwd
	    goto    pid_sum_ok
	
	btfss	BANKMASK(AdMotorPIDResH), 7 ; If we overflow in > 0 we have 
	    goto pid_send_zero_dc	    ; highly negative value and must
					    ; send 0 otherwise send max
    pid_send_max_dc:
	; If sum have overflowed DC is set to 100% then return
	banksel PSMC3DCH
	movlw	0x7F
	movwf	PSMC3DCH
	movlw	0xFF
	movwf	PSMC3DCL
	bsf	PSMC3LD
	return
	
    pid_sum_ok:
	; If PIDRes is < 0 the DC is set to 0% then return
	btfss	BANKMASK(AdMotorPIDResH), 7 ; if res < 0 we send 0 and are done.
	    goto send_Motor_dc
    pid_send_zero_dc:
	banksel PSMC3DCH
	clrf	PSMC3DCH
	clrf	PSMC3DCL
	bsf	PSMC3LD
	return
    send_Motor_dc:
	banksel PSMC3DCH
	movf	BANKMASK(AdMotorPIDResH), W
	movwf	PSMC3DCH
	movf	BANKMASK(AdMotorPIDResL), W
	movwf	PSMC3DCL
	bsf	PSMC3LD
    return
	

main_loop:
    call compute_pid
    clrwdt
    goto main_loop
END
