; MATLAB GENERATED FILE.
; See motor_dist_table.m
processor	pic16lf1789
GLOBAL	dist_table
GLOBAL	AdMotorVrefH
GLOBAL	AdMotorVrefL

psect dist_table__code,class=CODE,space=SPACE_CODE,delta=2
dist_table:
    andlw	00111111B
    brw
        goto	motor_0_cm
        goto	motor_1_cm
        goto	motor_2_cm
        goto	motor_3_cm
        goto	motor_4_cm
        goto	motor_5_cm
        goto	motor_6_cm
        goto	motor_7_cm
        goto	motor_8_cm
        goto	motor_9_cm
        goto	motor_10_cm
        goto	motor_11_cm
        goto	motor_12_cm
        goto	motor_13_cm
        goto	motor_14_cm
        goto	motor_15_cm
        goto	motor_16_cm
        goto	motor_17_cm
        goto	motor_18_cm
        goto	motor_19_cm
        goto	motor_20_cm
        goto	motor_21_cm
        goto	motor_22_cm
        goto	motor_23_cm
        goto	motor_24_cm
        goto	motor_25_cm
        goto	motor_26_cm
        goto	motor_27_cm
        goto	motor_28_cm
        goto	motor_29_cm
        goto	motor_30_cm
        goto	motor_31_cm
        goto	motor_32_cm
        goto	motor_33_cm
        goto	motor_34_cm
        goto	motor_35_cm
        goto	motor_36_cm
        goto	motor_37_cm
        goto	motor_38_cm
        goto	motor_39_cm
        goto	motor_40_cm
        goto	motor_41_cm
        goto	motor_42_cm
        goto	motor_43_cm
        goto	motor_44_cm
        goto	motor_45_cm
        goto	motor_46_cm
        goto	motor_47_cm
        goto	motor_48_cm
        goto	motor_49_cm
        goto	motor_50_cm
        goto	motor_51_cm
        goto	motor_52_cm
        goto	motor_53_cm
        goto	motor_54_cm
        goto	motor_55_cm
        goto	motor_56_cm
        goto	motor_57_cm
        goto	motor_58_cm
        goto	motor_59_cm
        goto	motor_60_cm
        goto	motor_61_cm
        goto	motor_62_cm
        goto	motor_63_cm
motor_0_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_1_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_2_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_3_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_4_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_5_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_6_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_7_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_8_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_9_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_10_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_11_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_12_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_13_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_14_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_15_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_16_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_17_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_18_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_19_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_20_cm:
        movlw	00000110B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001100B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_21_cm:
        movlw	00000101B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	01011101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_22_cm:
        movlw	00000101B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	01010001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_23_cm:
        movlw	00000101B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	01000101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_24_cm:
        movlw	00000101B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	00111001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_25_cm:
        movlw	00000101B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	00101101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_26_cm:
        movlw	00000101B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	00100001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_27_cm:
        movlw	00000101B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	00010101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_28_cm:
        movlw	00000101B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	00001001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_29_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	11111101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_30_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	11110001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_31_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	11100101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_32_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	11011001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_33_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	11001101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_34_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	11000001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_35_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10110101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_36_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10101001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_37_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10011101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_38_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10010001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_39_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10000101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_40_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	01111001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_41_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	01101101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_42_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	01100001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_43_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	01010101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_44_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	01001001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_45_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	00111101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_46_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	00110001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_47_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	00100101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_48_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	00011001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_49_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	00001101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_50_cm:
        movlw	00000100B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	00000001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_51_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	11110101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_52_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	11101001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_53_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	11011101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_54_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	11010001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_55_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	11000101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_56_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10111001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_57_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10101101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_58_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10100001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_59_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10010101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_60_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	10001001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_61_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	01111101B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_62_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	01110001B
        movwf	BANKMASK(AdMotorVrefL)
    return
motor_63_cm:
        movlw	00000011B
        movwf	BANKMASK(AdMotorVrefH)
        movlw	01100101B
        movwf	BANKMASK(AdMotorVrefL)
    return
END