; *********************************************************** ;
;                  PID FOR BOOST CONVERTER                    ;
;             Reads ADC value, compares with ref              ;
;		  and generates a duty cycle                  ;
;						              ;
;            APRI0007 - Major Project in electronics          ;
;                                                             ;
; *********************************************************** ;
 
    processor 16lf1789
    #include <xc.inc>

    thresh	EQU   20	; LED light-up threshold
    
    ; Global labels for the boost		
    GLOBAL boost_init
    GLOBAL boost_ADC_isr
    GLOBAL boost_timer1_isr
    
    ; Global labels for the sensor
    GLOBAL sensor_init
    GLOBAL sensor_timer2_isr
    GLOBAL sensor_ssp1_isr
    GLOBAL sensor_i2c_exec
    GLOBAL sensor_fetch_measurement
    GLOBAL last_dist
    
    ; Parameters for the configurations registers.
    config FOSC = INTOSC
    config WDTE = OFF
    config PWRTE = ON
    config MCLRE = ON
    config CP = OFF
    config CPD = OFF
    config BOREN = ON
    config CLKOUTEN = OFF
    config IESO = OFF
    config FCMEN = ON
    config WRT = OFF
    config PLLEN = OFF
    config STVREN = ON
    config BORV = LO
    config LPBOR = OFF
    config LVP = OFF
    
    wait_adc_acq MACRO
	REPT 32
	    nop
	ENDM
    ENDM
       
PSECT udata_bank1
    Flags:	DS 1
    ADCResL:	DS 1
    ADCResH:	DS 1
    
ADCResFull	EQU	0	; Bit 0 from Flags
ADCBoost_nAngle EQU	1	; Bit 1 from Flags
ADCRuning	EQU	2	; Bit 2 from Flags

    
; That is where the MCU will start executing the program (0x00)
psect code,abs
    startup: 
    org	    0x0
    nop
    goto    start		; jump to the beginning of the code
	
;Give the address of the interrupt routine to the interrupt vector
    org	    0x04 
    goto    interrupt

;BEGINNING OF THE PROGRAM
start:
    ; Configures the clock
    banksel	OSCCON
    movlw	11110000B	    ; SPLLEN = 1, IRCF = 1110, U, SCS = 00
    movwf	OSCCON		    ; configure oscillator (Fosc = 32MHz)
    movlw	00000000B	    
    movwf	OSCTUNE		    ; configure oscillator 
    
    ; Call all the sub-initialization routines
    call    boost_init		; boost initialisation routine
    call    sensor_init		; distance sensor initialisation routine 
    
    ; Enables global interrupts
    banksel INTCON
    bsf GIE			    ; Enables global interrupts
    bsf PEIE			    ; Enables peropheral interrupts
    
    goto    main_loop		; main loop

;INTERRUPT ROUTINE
interrupt:
    ; Check which subsystem throwed an interrupt
	banksel PIR1
	btfss	ADIF
	    goto test_tmr1if  
	    
	    goto ADC_isr
	
    test_tmr1if:
	btfss	TMR1IF		    ; Launch ADC for booster
	    goto test_ssp1if
	    
	; Check if ADC is free
	btfss   BANKMASK(Flags), ADCRuning
	    goto no_adc_running	; ADC Was not running
	btfsc   ADGO		; ADC is not free. Wait.
	    goto $-1
	movf    ADRESL, W
	movwf   BANKMASK(ADCResL)
	movf    ADRESH, W
	movwf   BANKMASK(ADCResH)
	bsf	BANKMASK(Flags), ADCResFull
	
	no_adc_running:
	btfsc	BANKMASK(Flags), Boost_nAngle
	    goto adc_already_set_for_boost
	; Set-up adc
	;; SELECT INPUT + WAIT ACQ TIME
	;; TODO
	
	adc_already_set_for_boost:
	goto boost_timer1_isr
	
    test_ssp1if:
	btfss	SSP1IF
	    goto test_tmr2if
	goto sensor_ssp1_isr
	
    test_tmr2if:
	btfss	TMR2IF		    ; Launch ADC for angle sensor
	    goto end_isr
	goto sensor_timer2_isr
	    
end_isr:	
    retfie  ; !!We should never retfie here. If this is the case there is an
	    ; INT that we do not manage and will be stuck in infinite isr loop!!
    
    
    
    	
main_loop:
    ; Call the sensor functions
    call sensor_fetch_measurement
    call sensor_i2c_exec
    
    ; Checks whether or not to light the LED
    banksel LATC
    movf last_dist, W
    sublw thresh
    
    btfss CARRY
    bcf RC5
    btfsc CARRY
    bsf RC5
    
    CLRWDT			    ; Clears watchdog timer
    goto main_loop
END startup