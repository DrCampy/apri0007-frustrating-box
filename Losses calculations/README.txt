File 0002 : Conduction losses in MOSFET (I and V)
File 0003 : Switching losses MOSFET (low -> High) (I and V)
File 0004: Switching losses MOSFET (high -> low) (I and V)
File 0005: Input (I and V)
File 0006: Current and voltage in coil
File 0007: Diode conduction losses (I and V)
File 0008: Diode switching losses (High-> Low) (I and V)
File 0009: Diode switching losses (Low -> High) (I and V)
File 0010: Output capacitor (I and V)
