close all
importall;
fs = 30000; %Switching frequency
%%Coil losses
    %One full period is for 2e-7 < t < 3.354e-5
    %mask creation
    mask = F6T>2e-7 & F6T<3.354e-5;
    %Mask without the spike:
    %mask = (F6T > 2e-7 & F6T < 1.83e-5) | (F6T > 2.112e-5 & F6T < 3.354e-5);


    %Computation of power for one period
    %F6Pm stands for F6, power, masked. CH2 is I, CH3 is V.
    F6Pm = F6CH2.*F6CH3.*mask;

    %Integrating the power over the period :
    coil_losses = trapz(F6T, F6Pm);
    coil_losses = coil_losses * fs
    figure;
    title('Losses of the coil');
    hold on;
    plot(F6T, F6CH2);
    plot(F6T, F6CH3);
    plot(F6T, F6Pm);
    legend('Current', 'Voltage', 'Power');

%%Mosfet losses
%%mosfet Switching losses (Low -> High) F0003
    %mask creation
    mask = F3T > 7.76e-8;

    %Computation of power for one period
    %F7Pm stands for F7, power, masked. CH2 is I, CH3 is V.
    F3Pm = abs(F3CH2.*F3CH3.*mask);

    %Integrating the power over the period :
    mos_switch_losses_low_to_high = trapz(F3T, F3Pm);
    mos_switch_losses_low_to_high = mos_switch_losses_low_to_high * fs    
    figure;
    title('Losses of the mosfet (switching, low to high)');
    hold on;
    plot(F3T, F3CH2);
    plot(F3T, F3CH3);
    plot(F3T, F3Pm);
    legend('Current', 'Voltage', 'Power');
    
%%mosfet Switching losses (High -> Low) F0004
    %mask creation
    mask = F4T > 1.04e-7;

    %Computation of power for one period
    %F7Pm stands for F7, power, masked. CH2 is I, CH3 is V.
    F4Pm = abs(F4CH2.*F4CH3.*mask);

    %Integrating the power over the period :
    mos_switch_losses_high_to_low = trapz(F4T, F4Pm);
    mos_switch_losses_high_to_low = mos_switch_losses_high_to_low * fs    
    figure;
    title('Losses of the Mosfet (switching, high to low)');
    hold on;
    plot(F4T, F4CH2);
    plot(F4T, F4CH3);
    plot(F4T, F4Pm);
    legend('Current', 'Voltage', 'Power');
disp('Total MOS switching losses: ');
total_mos_switching_losses = mos_switch_losses_high_to_low + mos_switch_losses_low_to_high;
disp(total_mos_switching_losses);
%%Diode conduction losses
    %One full period is for 2e-7 < t < 3.354e-5
    %Conduction period are
    % 2.88e-6;1,83e-5
    % 2.144e-5;3.352e-5
    %mask creation
    mask = (F7T > 2.88e-6 & F7T < 1.83e-5) | (F7T > 2.144e-5 & F7T < 3.354e-5);

    %Computation of power for one period
    %F7Pm stands for F7, power, masked. CH2 is I, CH3 is V.
    F7Pm = F7CH2.*F7CH3.*mask;

    %Integrating the power over the period :
    d_cond_losses = trapz(F7T, F7Pm);
    d_cond_losses = d_cond_losses * fs    
    figure;
    title('Losses of the diode (conduction)');
    hold on;
    plot(F7T, F7CH2);
    plot(F7T, F7CH3);
    plot(F7T, F7Pm);
    legend('Current', 'Voltage', 'Power');
    
%%Diode Switching losses (Low -> High) F0009
    %mask creation
    mask = F9T > 9.4e-8 & F9T < 2.138e-6;

    %Computation of power for one period
    %F7Pm stands for F7, power, masked. CH2 is I, CH3 is V.
    F9Pm = abs(F9CH2.*F9CH3.*mask);

    %Integrating the power over the period :
    d_switch_losses_low_to_high = trapz(F9T, F9Pm);
    d_switch_losses_low_to_high = d_switch_losses_low_to_high * fs    
    figure;
    title('Losses of the diode (switching, low to high)');
    hold on;
    plot(F9T, F9CH2);
    plot(F9T, F9CH3);
    plot(F9T, F9Pm);
    legend('Current', 'Voltage', 'Power');
    
%%Diode Switching losses (High -> Low) F0008
    %mask creation
    mask = F8T > 9e-8;

    %Computation of power for one period
    %F7Pm stands for F7, power, masked. CH2 is I, CH3 is V.
    F8Pm = abs(F8CH2.*F8CH3.*mask);

    %Integrating the power over the period :
    d_switch_losses_high_to_low= trapz(F8T, F8Pm);
    d_switch_losses_high_to_low = d_switch_losses_high_to_low * fs    
    figure;
    title('Losses of the diode (switching, high to low)');
    hold on;
    plot(F8T, F8CH2);
    plot(F8T, F8CH3);
    plot(F8T, F8Pm);
    legend('Current', 'Voltage', 'Power');
    
    diode_total_losses = d_cond_losses + d_switch_losses_low_to_high + d_switch_losses_high_to_low
    
%%Output capacitor losses
    %mask creation
    mask = F10T > 2e-7 & F10T < 3.354e-5;

    %Computation of power for one period
    %F7Pm stands for F7, power, masked. CH2 is I, CH3 is V.
    F10Pm = F10CH2.*F10CH3.*mask;

    %Integrating the power over the period :
    c_losses = trapz(F10T, F10Pm);
    c_losses = c_losses * fs    
    figure;
    title('Losses in the capacitor');
    hold on;
    plot(F10T, F10CH2);
    plot(F10T, F10CH3);
    plot(F10T, F10Pm);
    legend('Current', 'Voltage', 'Power');
    
disp('Total losses are');
total_losses = coil_losses + diode_total_losses + c_losses + total_mos_switching_losses;
disp(total_losses);

    