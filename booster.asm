; *********************************************************** ;
;                  PID FOR BOOST CONVERTER                    ;
;             Reads ADC value, compares with ref              ;
;		  and generates a duty cycle                  ;
;						              ;
;            APRI0007 - Major Project in electronics          ;
;                                                             ;
; *********************************************************** ;
   
 
    processor 16lf1789
#include	<p16lf1789.inc>
    
    ERRORLEVEL 	-302  ; To remove the "not in bank0" message
    
    ; Vref = 3V on ADC 12-bits:
    #DEFINE	VrefH	    b'00001110' 
    #DEFINE	VrefL	    b'10001011'
    #DEFINE	BoostPIDSat 0
    
    ; Parameters for the configurations registers.
    ; CONFIG1
    ; __config 0x2F82
     __CONFIG _CONFIG1, _FOSC_INTOSC & _WDTE_OFF & _PWRTE_ON & _MCLRE_OFF & _CP_OFF & _CPD_OFF & _BOREN_ON & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_ON
    ; CONFIG2
    ; __config 0x1EFF
     __CONFIG _CONFIG2, _WRT_OFF & _PLLEN_OFF & _STVREN_ON & _BORV_LO & _LPBOR_OFF & _LVP_OFF
    
    
; That is where the MCU will start executing the program (0x00)
	org 	0x00
	nop
	goto    start	 ; jump to the beginning of the code
	
	
;Give the address of the interrupt routine to the interrupt vector
	org 0x04 
	goto interrupt

;BEGINNING OF THE PROGRAM
start:
	call	initialisation    ; initialisation routine configuring the MCU
	goto	main_loop         ; main loop

;INITIALISATION
initialisation:
    ; Memory organisation:
    cblock 0xA0 ; replace number of adress in bank1 by a label
	Flags
	AdVrefH
	AdVrefL
	AdErrH ; error at time t
	AdErrL
	AdPrevH ; error at time t-1
	AdPrevL
	AdPropH ; Proportional term
	AdPropL
	AdCumH ; Integrational term
	AdCumL
	AdDiffH ; Derivative term
	AdDiffL
	AdPIDResH ; Sum of 3 terms
	AdPIDResL
    endc
    
    ; Enable interrupts:
	movlb	0x00
	
	clrf	PIR1 ; clear interrupt flags before enabling interupts.
	
	movlw   b'11000000' ;enable global and peripheral interupts.
	movwf   INTCON
	
	movlb	0x01
	movlw   b'01000001'
	iorwf	PIE1,1	; OR operation to not affect the other bits.
	
    ; Clear personnal flags:
	clrf	Flags

    ; I/O configuration
	banksel TRISA
	movlw   b'00000001'  
	movwf   TRISA      ; Only RD1 is an input (for ADC)       
	banksel LATA
	clrf	LATA    ; all outputs are zero         
	
    ; Configuration of clock - freq = 4MHZ - internal oscillator block 
	movlb	0x01
	movlw	b'01101110'
	movwf	OSCCON	    
	movlw	b'00000000'	    
	movwf	OSCTUNE	    
    
    ; Timer1 ON - 1:1 prescaling -  Fosc/1 clock source
	movlb	0x00
	movlw	b'01000001'
	movwf	T1CON       ; configure Timer1 (cf. datasheet SFR T1CON)
	
    ; Configuration of ADC:
	movlb	0x01
	movlw   b'00000001' ; 12 bits result, enabled
	movwf	ADCON0 
	movlw   b'11000000' ; 2's Complement -  Fosc/4 - Vref = VDD/VSS
	movwf	ADCON1 ;
	
    ; Configuration of PWM signal:
	; Set period to 33.33�s with a 64MHz clk:
	    banksel PSMC1CON
	    movlw   b'00001000'
	    movwf   PSMC1PRH
	    movlw   b'01010101'
	    movwf   PSMC1PRL
	; No offset:
	    clrf    PSMC1PHH   
	    clrf    PSMC1PHL
	; Initialize with D = 0% [rising edge = falling edge]:
	    clrf    PSMC1DCH   
	    clrf    PSMC1DCL 
	; Clock frequency = 64MHz:
	    movlw   0x01 
	    movwf   PSMC1CLK
	; Set output on A with normal polarity:
	    bsf	    PSMC1STR0,	P1STRA
	    bcf	    PSMC1POL,	P1POLA
	    bsf	    PSMC1OEN,	P1OEA
	; Set time base event for all events:
	    bsf	    PSMC1PRS,	P1PRST
	    bsf	    PSMC1PHS,	P1PHST	
	    bsf	    PSMC1DCS,	P1DCST
	; Enable the PWM and the output pin:
	    movlw   b'11000000' ; No deadbands and single PWM waveform
	    movwf   PSMC1CON
	    banksel TRISC
	    bcf	    TRISC, 0 ; pinA of PSMC1 corresponds to RC0
	
    ; Initialize Timer1 to trigger interrupt at 200�s [f=5kHz]                     
	movlb   0x00	
	bcf	T1CON, TMR1ON
	movlw	b'11111100'
	movwf	TMR1H
	movlw	b'11100000'
	movwf	TMR1L
	bsf	T1CON, TMR1ON
	
    ; Storing reference in memory:
	movlb   0x01
	movlw   VrefH
	movwf   AdVrefH	
	movlw   VrefL
	movwf   AdVrefL
    
    ; Setting previous error to 0 for D part of PID
	clrf   AdPrevH
	clrf   AdPrevL
    
    ; Setting cumulated error to 0 for I part of PID
	clrf  AdCumH
	clrf  AdCumL
    
    return

;INTERRUPT ROUTINE
interrupt:
    
    movlb   0x00
    btfss   PIR1, TMR1IF   
    ; Check ADC if the Timer1 overflow flag is not set
	goto	ADC_interrupt

	bcf	PIR1, TMR1IF
    
    ; Reset the counter to get next interrupt 200�s later (f = 5kHz)
	movlb   0x00	
	bcf	T1CON, TMR1ON
	movlw	b'11111100'
	movwf	TMR1H
	movlw	b'11100000'
	movwf	TMR1L
	bsf	T1CON, TMR1ON
    
    ; Start ADC conversion
	movlb	0x01
	bsf	ADCON0, 1 
    
ADC_interrupt:
    movlb 0x00
    btfss PIR1, ADIF 
    ; Leave interrupt routine if ADC flag is not set
	goto end_of_interrupt
    
    bcf	  PIR1, ADIF ; Clear ADC interrupt flag
    
    ; Computation of the error e = Vref - Vadc:
	movlb	0x01
	movf    ADRESL, 0   
	subwf   AdVrefL, 0
	movwf   AdErrL
	movf    ADRESH, 0   
	subwfb  AdVrefH, 0
	movwf   AdErrH  
    
    ; PID Calculation:
	; Kp = 0.015625 -> Kp* ~= 2^-5 (Adjusted to digital) 
	movf    AdErrH,	0 
	movwf   AdPropH
	movf    AdErrL,	0 
	movwf   AdPropL
	asrf	AdPropH, 1  ; We do it five times (faster than loop)
	rrf	AdPropL, 1
	asrf	AdPropH, 1
	rrf	AdPropL, 1
	asrf	AdPropH, 1
	rrf	AdPropL, 1
	asrf	AdPropH, 1
	rrf	AdPropL, 1
	asrf	AdPropH, 1
	rrf	AdPropL, 1
	
	; Ki * Int:
	;if sat = 1, we directly jump to the derivative term
	btfsc	Flags, BoostPIDSat
	goto	derivative_term
	
	; Ki*e[k] = Kp*e[k]: we can just add the proportional term 
	; to the cumulative sum
	movf	AdPropL, 0
	addwf	AdCumL, 1
	movf	AdPropH, 0
	addwfc	AdCumH, 1
	    
derivative_term:
	;Kd = 0.125 -> Kd* ~= 2^-2 (Adjusted to digital) 
	; Calculation of DeltaE[k] = e[k]- e[k-1]:
	movf	AdPrevL, 0
	subwf	AdErrL, 0
	movwf	AdDiffL
	movf	AdPrevH, 0
	subwfb	AdErrH, 0
	movwf	AdDiffH
	; Calculation of Kd * DeltaE[k]:
	asrf	AdDiffH, 1  ; We do it two times (faster than loop)
	rrf	AdDiffL, 1
	asrf	AdDiffH, 1
	rrf	AdDiffL, 1
	
	
	; Summate the three terms:
	movf	AdCumL, 0
	addwf	AdDiffL, 0
	movwf	AdPIDResL
	movf	AdCumH, 0
	addwfc	AdDiffH, 0
	movwf	AdPIDResH
	
	btfsc	STATUS, C	;check overflow
	goto	positive_sat
	
	movf	AdPropL, 0
	addwf	AdPIDResL, 1
	movf	AdPropH, 0
	addwfc	AdPIDResH, 1
	
	btfsc	STATUS, C	;check overflow
	goto	positive_sat
	    
    
    ; Duty cycle generation:
	; Check if PIDRes is negative: there was no overflow, if bit7=1 -> negative
	    btfss   AdPIDResH, 7
	    goto    positive_boost_duty
	    
	; if the MSB is 1, the output of controller is negative => D = 0:
	    bsf	    Flags, BoostPIDSat  ; Anti-windup
	    banksel PSMC1CON
	    clrf    PSMC1DCH   
	    clrf    PSMC1DCL 
	    goto    update_prev_error
	
	; else:	
positive_boost_duty:	    
	    movf    AdPIDResH, 0
	    banksel PSMC1CON
	    subwf   PSMC1PRH, 0
	    
	    btfsc   STATUS, C   
	    goto    clear_boost_windup
	    
positive_sat:
	    ; if C = 0: PIDOutput > PeriodPSMC1 => sat = 1
	    movlw   0x01
	    bsf	    Flags, BoostPIDSat  ; Anti-windup on
	    ;update duty-cycle to bigger value than period to have DC = 100%:
	    banksel PSMC1CON
	    movlw   b'11111111'
	    movwf   PSMC1DCH
	    goto    update_prev_error
	    
	    ; else: sat = 0
clear_boost_windup: 
		movlw	0x01
		bcf	Flags, BoostPIDSat  ; Anti-windup off
	    
	
update_duty:    
	    ; Store the output of PID in Duty-cycle
	    movf    AdPIDResH, 0
	    banksel PSMC1CON
	    movwf   PSMC1DCH 
	    movlb   0x01
	    movf    AdPIDResL, 0
	    banksel PSMC1CON
	    movwf   PSMC1DCL
    
update_prev_error:
    ; Move error in prevError location for next computation:
	movlb	0x01
	movf	AdErrH,	0 
	movwf   AdPrevH
	movf    AdErrL,	0 
	movwf   AdPrevL  
    
end_of_interrupt:
    retfie
    
 	
;MAIN LOOP
main_loop:
    nop
    CLRWDT
    goto main_loop
    END  
;******************************************************************; 